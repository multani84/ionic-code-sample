angular.module('starter.controllers').controller('SinglePostCtrl', function($scope, $ionicLoading, $stateParams, $ionicHistory, $ionicPopup, $state, $http, jobdetails, $rootScope, $ionicListDelegate, Reciever, userdetails, $ionicPopup, dbservice, $timeout) {
	
	var user_id = userdetails.getUserId();  
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}

	$scope.jobedit = false;

	$scope.loadingTrackers = true;
	$scope.shouldShowDelete = false;
	$scope.shouldShowReorder = false;
	$scope.listCanSwipe = true;

	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	$scope.noaplicant = false;

	$scope.showpaynow = false;

	var jobid = $stateParams.jobId;

	$scope.job_type = "Hourly";

	$scope.paidamount = $scope.dueamount = $scope.profileid = 0;
	
	var api_token = userdetails.getTokenId();

	dbservice.getJobDetails(jobid, user_id).then(function(response){
		
		$ionicLoading.hide();

		if(response == -1){
			var errorviewjob = $ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "Due to some technical issues we are unable to process your request. Please try again later."
			}); 

			errorviewjob.then(function(res) {
				$state.go("app.map", {reload: true});
			});

		} else {
		
			if(response.ResponseCode== true){ 
				var data = response.data;

				if(data.status==1){
					$scope.jobedit = true;
				} else {
					$scope.jobedit = false;
				}

				Reciever.setDataUp(data);

				$scope.job_update_id=jobid;

				$scope.loadingTrackers = false;
				$scope.job_title=data.job_title;
				$scope.description=data.description;
				$scope.image=data.image;
				$scope.address=data.address;
				$scope.name=data.name;
				$scope.start_date=data.start_date;
				$scope.end_date=data.end_date;
				$scope.start_time=data.start_time;
				$scope.end_time=data.end_time;
				$scope.owner_fname=data.user.first_name;
				$scope.owner_lname=data.user.last_name;
				$scope.added_by=data.added_by;
				$scope.status=data.status;
				$scope.catgoryname = data.category.name;
				$scope.job_type = data.job_type;

				$scope.ownerimage = response.baseurl + "" +data.user.profile_picture;

				if(data.job_type == "fixed" || data.job_type == "Fixed"){
					$scope.cost = data.cost + " kr";
				} else {
					$scope.cost = data.cost + " kr /h";
				}

				dbservice.getJobApplicants(jobid, api_token).then(function(responseapplicants){
					
					if(responseapplicants.ResponseCode== true){
						var applicantdata = responseapplicants.data;
						var job_status=$scope.status;
						var ishiredalready = 0;
						
						if(applicantdata.length >0){
							$scope.noaplicant = false;
						} else {
							$scope.noaplicant = true;
						}

						for (var i in applicantdata){
							applicantdata[i].hirebutton = false;
							applicantdata[i].endbutton = false;

							var hired='';
							$scope.noaplicant=false;
							$scope.showapplicant=true;
					
							if(applicantdata[i].status== 1 && job_status==3){
								applicantdata[i].status="Completed";
								$scope.listCanSwipe = false;
								applicantdata[i].status_code = 3;

								dbservice.getJobSummary(jobid, applicantdata[i].user_id, "mypost", api_token).then(function(responsesummary){
									if(responsesummary != -1){
										if(responsesummary.ResponseCode== true){ 
											$scope.awardedapplied = true;
											$scope.paidamount = responsesummary.data.paidamount;
											$scope.dueamount = responsesummary.data.dueamount;

											if(responsesummary.data.dueamount >0){
												$scope.appliedbuttonclass = "button-assertive";
												$scope.showpaynow = true;
												$scope.profileid = responsesummary.data.pay_user_id;
											}
										}
									}
								});

							}else if(applicantdata[i].status==1){
							  applicantdata[i].status="Hired";
							  applicantdata[i].endbutton = true;
							  ishiredalready = 1;
							  applicantdata[i].status_code = 1;

							  dbservice.getJobSummary(jobid, applicantdata[i].user_id, "mypost", api_token).then(function(responsesummary){
									if(responsesummary != -1){
										if(responsesummary.ResponseCode== true){ 
											$scope.awardedapplied = true;
											$scope.paidamount = responsesummary.data.paidamount;
											$scope.dueamount = responsesummary.data.dueamount;

											if(responsesummary.data.dueamount >0){
												$scope.appliedbuttonclass = "button-assertive";
												$scope.showpaynow = true;
												$scope.profileid = responsesummary.data.pay_user_id;
											}
										}
									}
								});

							}else if(applicantdata[i].status== 0){
							  applicantdata[i].status="Applied";
							  if(ishiredalready ==0)
							  applicantdata[i].hirebutton = true;
							  applicantdata[i].status_code = 0;
							}

							applicantdata[i].count = 0;

							applicantdata[i].job_id = jobid;
							listarray.push(applicantdata[i]);
					  
							$scope.loadingTrackers = false;
							$scope.seekers = listarray;
							$scope.hired=hired;
							$scope.noaplicant=false;
							$scope.showapplicant=true;
					  
							if(listarray.length==0){
								$scope.noaplicant=true;
								$scope.showapplicant=false;
							}
						}

						
					} else if(responseapplicants.ResponseCode == false){
						$scope.noaplicant=true;
						$scope.showapplicant=false; 
					}
				});
				
				


			} else if(response.ResponseCode== false){
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				});  
			}
		}
	});

	$scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {

		var api_token = userdetails.getTokenId();

        for (var i in $scope.seekers){
			 dbservice.getJobApplicantMessagesCount(jobid, $scope.seekers[i].user_id, i, api_token).then(function(response){
				if(response.data >0){	
					console.log(response);
					$scope.seekers[response.indexid].count = response.data;
				}
			});
		}
    });
    
    var listarray=[];

    var usrname='';
     
	$scope.noaplicant=false;
	$scope.showapplicant=false;

    $scope.endjob=function(item){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		$ionicListDelegate.closeOptionButtons();
		
		$scope.listCanSwipe = false;

        user_id = userdetails.getUserId();

		dbservice.endJob(item.job_id, user_id, api_token).then(function(response){
			$ionicLoading.hide();
			if(response == -1){
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				}); 
				
			} else {
				if(response.ResponseCode==true){ 
					item.status = "Completed";
					$ionicListDelegate.closeOptionButtons(); 
					$scope.listCanSwipe = false;
					$state.go("app.singlemypost",{jobId: item.job_id}, {reload: true});
				} else {
					$ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: response.Message
					}); 
				}
			}
		});
         
    }

    $scope.hire=function(item){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
      		
		dbservice.hireUserForJob(item.job_id, item.user_id, api_token).then(function(response){
			$ionicLoading.hide();
			if(response == -1){
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				});
			} else {
				if(response.ResponseCode==true){
					item.status = "Hired";
					$ionicListDelegate.closeOptionButtons(); 
					item.hirebutton = false;
					item.endbutton = true;

					$state.go("app.singlemypost",{jobId: item.job_id}, {reload: true});

				} else {
					$ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: response.Message
					}); 
				}
			}
		});

		
    }  

	$scope.viewprofile=function(item){
		$state.go("app.profilejob", {profileid: item.user_id, jobid: item.job_id}, {reload: true});
	}

	$scope.UpdateProfile=function(){
		var job_up = $scope.job_update_id;
		$state.go("app.updatejob",{jobidupdate:job_up});
	}

	$scope.goChat=function(item){
		$state.go("app.chat",{id:item.user_id, jobid: item.job_id});
	}

	$scope.payNow=function(){
		dbservice.getDueAmount(jobid, api_token).then(function(response){
			if(response.data.ResponseCode==true){
				$state.go("app.pay_rate",{dueAmount: response.data.data, jobid: jobid, profileid: $scope.profileid});
			} else {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: response.data.Message
				});
			}
		});
	}
})