angular.module('starter.controllers').controller('SingleJobCtrl', function($scope, $ionicLoading, $location, $rootScope, $stateParams, $state, myURL, $http, $timeout, jobdetails, $ionicHistory, $ionicPopup, localStorageService, userdetails, dbservice) {
	
	var jobid = $stateParams.jobId;

	localStorageService.set("job_id_redirect", jobid);

	var userInfo = userdetails.getLoggedUserDetails();
	
	var user_id = 0;

	if( userInfo != 0){
		user_id = userInfo.id;
		$scope.canapply = false;
	} else {
		$scope.canapply = true;
	}
	

	if(userInfo.user_job_role == 2){
		$scope.canapply = true;
	}
	
	$scope.$on('userlogout', function (event, args) {
		$scope.contentclass = "";
		$scope.$parent.bottomtabs = false;
		$scope.$parent.showmenubutton = false;
	});

	if( user_id >0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
		$scope.contentclass = "has-tabs";
		$scope.$parent.showloginicon = false;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
		$scope.contentclass = "";
		$scope.$parent.showloginicon = true;
	}

	$scope.appliedstatus = false;

	$scope.job_type = "Fixed";

	$scope.appliedbuttonclass = "button-energized";

	$scope.appliedbuttontext = "In Progress";

	$scope.paidamount = $scope.dueamount = 0;

	$scope.ownerimage = "";
	
	$scope.image = "";
	
	dbservice.getJobDetails(jobid, user_id).then(function(response){

		if(response != -1){
    
			$ionicLoading.hide();

			if(response.ResponseCode== true){ 
				var data = response.data;

				$scope.job_title=data.job_title;
				$scope.description=data.description;
				$scope.image=data.image;
				$scope.address=data.address;
				$scope.name=data.name;
				$scope.start_date=data.start_date;
				$scope.end_date=data.end_date;
				$scope.start_time=data.start_time;
				$scope.end_time=data.end_time;
				$scope.owner_fname=data.user.first_name;
				$scope.owner_lname=data.user.last_name;
				$scope.added_by=data.added_by;
				$scope.catgoryname = data.category.name;
				$scope.job_type = data.job_type;
				$scope.cost = data.cost;

				if(data.job_type == "fixed" || data.job_type == "Fixed"){
					$scope.cost = data.cost + " kr";
				} else {
					$scope.cost = data.cost + " kr /h";
				}

				$scope.alreayapplied = false;

				$scope.ownerimage = response.baseurl + "" +data.user.profile_picture;

				if(user_id > 0){

					var api_token = userdetails.getTokenId();

					if($scope.added_by == user_id){
						$scope.appliedstatus = false;
					} else if(data.hasapplied == 1) {
						$scope.appliedstatus = false;
						$scope.alreayapplied = true;
						
						var applieddata = data.applieddata[0];

						console.log(applieddata);

						if(applieddata.status == "1"){ // Job Awarded

							

							if(data.status == 3){ // completed
								$scope.appliedbuttonclass = "button-balanced";
								$scope.appliedbuttontext = "Completed";

								dbservice.getJobSummary(jobid, user_id, 'userjob', api_token).then(function(responsesummary){
									if(responsesummary != -1){
										if(responsesummary.ResponseCode== true){ 
											$scope.awardedapplied = true;
											$scope.paidamount = responsesummary.data.paidamount;
											$scope.dueamount = responsesummary.data.dueamount;

											if(responsesummary.data.dueamount >0){
												$scope.appliedbuttonclass = "button-assertive";
											}
										}
									}
								});

							} else {
								$scope.appliedbuttonclass = "button-energized";
								$scope.appliedbuttontext = "In Progress";
							}

							
						} else {
							$scope.appliedbuttonclass = "button-balanced";
							$scope.appliedbuttontext = "Already Applied";
						}

					} else {
						$scope.appliedstatus = true;
					}

				} else {
					$scope.appliedstatus = true;
				}

				

			} else if(response.ResponseCode== false){
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				});      
			}
		} else {
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "Due to some technical issues we are unable to process your request. Please try again later."
			});
		}
	});

	$scope.ApplyJob=function(){
		
		user_id = userdetails.getUserId();

		if(user_id >0){
			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			
			var api_token = userdetails.getTokenId();

			dbservice.applyUserForJob(jobid, user_id, api_token).then(function(response){
			
				$ionicLoading.hide();

				if(response != "-1"){
					var data = response.data;
					//alert(JSON.stringify(response.data));
					if(response.data.ResponseCode==true){
						var successapply = $ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: response.data.Message
						});
						successapply.then(function(res) {
							$state.go("app.appliedjobs", {reload: true});
						});

					} else {
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: response.data.Message
						});
					}
				} else {
					$ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: "Due to some technical issues we are unable to process your request. Please try again later."
					});
				}
			}); 

		} else {
			$ionicHistory.nextViewOptions({
			  disableAnimate: true,
			  disableBack: false
			});

			$state.go("app.login", {}, "reload");
		}
	}

	$scope.viewOwnerProfile=function(){
		$state.go("app.profile", {profileid:$scope.added_by});
	}

});