angular.module('starter.controllers').controller('ChangePasswordCtrl', function($scope, $ionicLoading, dbservice, $ionicPopup, $state, localStorageService, userdetails) {
  
	var userdata = localStorageService.get("userdetails");

	var user_id = userdata.id;
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}
	
	$scope.passwordtext = "Change Password";

	$scope.user = [];

	$scope.needOldPassword = false;

	$scope.loadingTrackers = true;

	dbservice.getUserDetails(user_id).then(function(response){
		if(response != -1){
			
			if(response.ResponseCode == true){

				if(response.data.userDetail.password == "" && response.data.userDetail.facebook_id != ""){
					$scope.needOldPassword = false;
					$scope.passwordtext = "Set Password";
				} else {
					$scope.needOldPassword = true;
					$scope.passwordtext = "Change Password";
				}
				$scope.loadingTrackers = false;
				$scope.changeFormShow = true;
			}
		} else {
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "Due to some technical issues we are unable to process your request. Please try again later."
			});
		}
	});

	$scope.change_pwd_profile=function(password){
		
     
		var old_password	=	"";

		var oldpwd		=	password.oldpassword;

		var confirm_pwd	=	password.confirmpassword;

		var new_password	=	password.newpassword;
		
		if($scope.needOldPassword){
			if(confirm_pwd == "" || new_password ==" " || oldpwd == ""){
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Please input all fields."
				});
			} 
		} else {
			if(confirm_pwd == "" || new_password ==" "){
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Please input all fields."
				});
			} 
		}


		if(confirm_pwd==new_password){
			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			
			if($scope.needOldPassword){

				var api_token = userdetails.getTokenId();

				dbservice.changePassword(user_id, oldpwd, new_password, api_token).then(function(response){
					$ionicLoading.hide();
					if(response.data.ResponseCode==true){
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "Password has been changed"
						});	
					} else{
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "Please check your old password"
						});	
					}
				});
			} else {
				
				var useremail = userdata.email;

				dbservice.resetPassword(useremail, new_password).then(function(response){
					$ionicLoading.hide();
					if(response.data.ResponseCode==true){
						var successsignup = $ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "Your password has been reset."
						});
						successsignup.then(function(res) {
							$ionicHistory.nextViewOptions({
							  disableAnimate: true,
							  disableBack: true
							});
							$state.go("app.login", {reload: true});
						});
					} else {
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: response.data.Message
						});
					}
				});
			}

		} else {
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "New password & Confirm password should match"
			});
		}
	
	}
              
  })