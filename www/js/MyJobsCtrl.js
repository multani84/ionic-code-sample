angular.module('starter.controllers').controller('MyJobsCtrl', function($scope, $stateParams, $ionicLoading, $state, userdetails, dbservice, $timeout) {

    $scope.noresults = false;
	$scope.editSearchMyPosts = true;

	var user_id = userdetails.getUserId();  
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}

	var listarray = [];

	$scope.archivedLists = false;

	$scope.listCanSwipe = true;
    $scope.loadingTrackers = true;

	$scope.keyword = "";

	$scope.getjobs = function(keyword){
		
		var api_token = userdetails.getTokenId();

		dbservice.getMyJobs(user_id, keyword, -1, api_token).then(function(response){
	
			$scope.loadingTrackers = false;

			$ionicLoading.hide();

			listarray = [];

			if(response.data.ResponseCode== true){ 
				var data = response.data.data;
				for (var i in data){
					
					data[i]['showremove'] = true;

					if(data[i].status == 3){ // Completed

						if(data[i].dueamount >0){
							data[i].jobstatus = "item item-complex item-right-editable item-left-editable completed-unpaid";
						} else {
							data[i].jobstatus = "item item-complex item-right-editable item-left-editable completed-paid";
						}

						data[i].jobstatusstr = "Completed";

						data[i]['showremove'] = false;
						data[i]['showedit'] = false;

					} else if(data[i].status == 2){ // Hired
						data[i].jobstatus = "item item-complex item-right-editable item-left-editable inprogress";
						data[i].jobstatusstr = "Inprogress";
						data[i]['showremove'] = false;
						data[i]['showedit'] = false;
					} else {
						data[i].jobstatus = "item item-complex item-right-editable item-left-editable";
						data[i].jobstatusstr = "Open";
						data[i]['showremove'] = true;
						data[i]['showedit'] = true;
					}

					for(var j in data[i].jobsapplied){
						if(data[i].jobsapplied[j].status == 1){
							data[i].hired_user_id = data[i].jobsapplied[j].user_id;
						}
					}

					
					data[i].count = 0;

					listarray.push(data[i]);
				}


				$scope.noresults = false;
				
				if(listarray.length==0){
					$scope.noresults = true;
				}
			} else {
				$scope.noresults = true;  
			}
			
			$scope.items = listarray;
			
		});
	};

	$scope.getjobs("");


	$scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {

		var api_token = userdetails.getTokenId();

		for (var i in $scope.items){
			 dbservice.getJobMessagesCount($scope.items[i].id, user_id, i, api_token).then(function(response){
				if(response.data >0){
					$scope.items[response.indexid].count = response.data;
				}
			});
		}
    });

	$scope.goSeeApplicant=function(item){
		$state.go("app.singlemypost", {jobId: item.id}, {reload: true});
	}

	$scope.edit_job = function(item){
		$state.go("app.updatejob", {jobidupdate: item.id}, {reload: true});
	}

	$scope.remove_job=function(item, index){

		user_id = userdetails.getUserId();  
		
		var api_token = userdetails.getTokenId();

		$scope.items.splice(index, 1);

		dbservice.removeJob(user_id, item.id, api_token).then(function(response){
			$ionicLoading.hide();
		});
		
		return true;
	}

	$scope.SearchMyJobChange=function(keyword){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		$scope.editSearchMyPosts = true;

		$scope.getjobs(keyword);
	} 

	$scope.doRefresh = function() {
		$scope.archivedLists = true;
		$scope.$broadcast('scroll.refreshComplete');
	};

	
})