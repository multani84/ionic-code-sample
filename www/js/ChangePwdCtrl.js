angular.module('starter.controllers').controller('ChangePwdCtrl', function($scope, $ionicLoading, dbservice, $state, $rootScope, $ionicPopup, $ionicHistory, localStorageService, userdetails) {
 
	$scope.change_password=function(changepwd){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		var oldpwd			=	changepwd.old_pwd;
		var new_password	=	changepwd.new_pwd;
		var emailaddress	= localStorageService.get("verifymail");

		if(oldpwd == new_password){

			dbservice.resetPassword(emailaddress, new_password).then(function(response){
				$ionicLoading.hide();
				if(response == -1){
					$ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: "Due to some technical issues we are unable to process your request. Please try again later."
					});
				} else {
					if(response.data.ResponseCode==true){
						var successsignup = $ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "Your password has been reset. You can now login with your new password"
						});
						successsignup.then(function(res) {
							$ionicHistory.nextViewOptions({
							  disableAnimate: true,
							  disableBack: true
							});
							$state.go("app.login", {reload: true});
						});
					} else {
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: response.data.Message
						});
					}
				}
			});
           
        }else{
			$ionicLoading.hide();
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "Both passwords should match"
			});
        }   
	}
})