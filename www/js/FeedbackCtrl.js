angular.module('starter.controllers').controller('FeedbackCtrl', function($scope, $stateParams, $ionicPopup, $state, dbservice, userdetails) {
    
	var jobid = $stateParams.jobid;

	var profileid = $stateParams.uid;

	$scope.feed = [];

	$scope.feed.jobid = jobid;
	$scope.feed.profileid = profileid;

	$scope.feed.ratings = 4;

	var user_id = userdetails.getUserId();  

	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}


	$scope.ratingsObject = {
		iconOn: 'ion-ios-star', //Optional
		iconOff: 'ion-ios-star-outline',  //Optional
		iconOnColor: 'rgb(200, 200, 100)',  //Optional
		iconOffColor: 'rgb(200, 100, 100)', //Optional
		rating: 4,  //Optional
		minRating: 1, //Optional
		readOnly:false, //Optional
		callback: function(rating) {  //Mandatory    
			$scope.ratingsCallback(rating);
		}
	};

	$scope.ratingsCallback = function(rating) {
		$scope.feed.ratings = rating;
	};
      
	$scope.givefeedback=function(feeditem){
		
		var api_token = userdetails.getTokenId();

		dbservice.giveFeedback(user_id, profileid, jobid, feeditem.ratings, feeditem.feedback, api_token).then(function(response){
			
			if(response != -1){
				if(response.ResponseCode==true){
					var successsignup = $ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: "Your feedback has been saved."
					});
					successsignup.then(function(res) {
						$state.go("app.singlemypost",{jobId: jobid});
					});
				} else {
					$ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: response.Message
					});	
				}
			} else {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				});
			}
	
		});
	}
})