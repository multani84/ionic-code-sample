angular.module('starter.controllers').controller('MapCtrl', function($scope, $cordovaGeolocation, $ionicHistory, $ionicLoading, $state, $ionicSideMenuDelegate, $rootScope, dbservice, userdetails, $timeout) {
	
	var user_id = userdetails.getUserId(); 
	
	$scope.contentclass = "";

	$scope.hasFilters = true;

	$scope.keyword = "";

	$scope.lat = 0;
	$scope.log = 0;

	$scope.hasFilters = false;

	$scope.noresults = false;

	$scope.openFilters = function(hasFilters){
		if(hasFilters){
			var keyword = $scope.keyword;
			$scope.hasFilters = false;
			if(keyword != ""){
				$scope.keyword = "";
				initialize($scope.lat, $scope.log, 0);
			}
		} else {
			$scope.hasFilters = true;
		}
	}

	$scope.searchJobs = function(search){
		$scope.keyword = search;
		initialize($scope.lat, $scope.log, 0);
	}

	$rootScope.$on('settingsintialized', function (event, args) {
		$scope.appname = $rootScope.appname;
	});

	$scope.$on('userlogout', function (event, args) {
		console.log("User Logged Out Map");
		$scope.contentclass = "";
		$scope.$parent.bottomtabs = false;
		$scope.$parent.showmenubutton = false;
		$scope.$parent.showloginicon = true;
	});

	$scope.alljobs = [];

	if( user_id >0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
		$scope.contentclass = "has-tabs";
		$scope.$parent.showloginicon = false;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
		$scope.contentclass = "";
		$scope.$parent.showloginicon = true;
	}

	var startPos;
	var geoOptions = {
		timeout: 10 * 1000
	}

	$scope.loadingTrackers=true;

	var geoSuccess = function(position) {
		startPos = position;
		$scope.lat = startPos.coords.latitude;
		$scope.log = startPos.coords.longitude;
		initialize($scope.lat, $scope.log, 0);
	};
	var geoError = function(error) {
		//alert('Error occurred. Error code: ' + error.code);
		$scope.lat = 55.6761;
		$scope.log = 12.5683;
		initialize($scope.lat, $scope.log, 0);
		// error.code can be:
		//   0: unknown error
		//   1: permission denied
		//   2: position unavailable (error response from location provider)
		//   3: timed out
	};

	var options = {timeout: 10000, enableHighAccuracy: true};

	$cordovaGeolocation.getCurrentPosition(options).then(function(position){
		var startPos = position;
		$scope.lat = startPos.coords.latitude;
		$scope.log = startPos.coords.longitude;
		initialize($scope.lat, $scope.log, 0);
	}, function(error){
		$scope.lat = 55.6761;
		$scope.log = 12.5683;
		initialize($scope.lat, $scope.log, 0);
	});
	
	//navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);

	var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var labelIndex = 0;
	
	var imageurl = "";

	function initialize(lat,lon, category) {
    
		var myLtLg = new google.maps.LatLng(lat, lon);
		
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 10,
			center: myLtLg,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});

		var limit=100; 
	
		var keyword = $scope.keyword;

		dbservice.getAllJobs(lat, lon, user_id, limit, keyword, category).then(function(response){
			
			$scope.loadingTrackers=false;

			if(response != -1){
				var jobArray=[];
				var data = response.data.data;

				imageurl = response.data.mapimageurl;

				$scope.alljobs = data;

				$ionicLoading.hide();

				for (i = 0; i < data.length; i++){
					if(i<=4){
					jobArray.push(data[i]);
					}

					var LatLong= { lat: parseFloat(data[i].lat), lng: parseFloat(data[i].lng) };
					
					addMarker(LatLong,map,data[i], imageurl);
				}
			
				if(jobArray.length <=0 ){
					$scope.noresults = true;
				} else {
					$scope.noresults = false;
				}


				$scope.items = jobArray;
				
			}
		}); 
			
		
	}
	

	function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
		var R = 6371; // Radius of the earth in km
		var dLat = deg2rad(lat2-lat1);  // deg2rad below
		var dLon = deg2rad(lon2-lon1); 
		var a = 
		Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
		Math.sin(dLon/2) * Math.sin(dLon/2)
		; 
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		var d = R * c; // Distance in km
		return d;
	}

	function deg2rad(deg) {
		return deg * (Math.PI/180)
	}
	
	var infoWindow = null;

	function addMarker(location, map, data, iconurl) {
		
		var iconurlnn = iconurl+"other.png";
		
		if(data.category != null){
			iconurlnn = iconurl+data.category.image
		}

		var marker = new google.maps.Marker({
			position: location,
			//label: labels[labelIndex++ % labels.length],
			id: data.id,
			data: data,
			map: map,
			icon: {
				url: iconurlnn,
				// This marker is 20 pixels wide by 32 pixels high.
				size: new google.maps.Size(100, 100),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(40, 40)
			},
			title: data.job_title
		}, function(marker) {
			showInfoWindow();
		});
		

		marker.addListener('click', function (item) {
			
			if (infoWindow) {
				infoWindow.close();
			}
			
			infoWindow = new InfoBubble({
			  map: map,
			  content: marker.get("data").job_title+"<br/>",
			  //position: new google.maps.LatLng(-35, 151),
			  shadowStyle: 1,
			  padding: 2,
			  backgroundColor: '#fff',
			  borderRadius: 4,
			  arrowSize: 10,
			  borderWidth: 1,
			  borderColor: '#fff',
			  disableAutoPan: false,
			  hideCloseButton: true,
			  arrowPosition: 30,
			  backgroundClassName: 'phoney',
			  arrowStyle: 2
			});

			/*infoWindow = new google.maps.InfoWindow({
				content: marker.get("data").job_title
			});*/

			map.setCenter(marker.getPosition());

			infoWindow.open(map, marker);

			var jobdata = marker.get("data");
			
			var distance = 0;
			
			var neardistancejob = [];
			
			for(var i in $scope.alljobs){
				var lat1 = jobdata.lat;
				var lon1 = jobdata.lng;

				var lat2 = $scope.alljobs[i].lat;
				var lon2 = $scope.alljobs[i].lng;

				var jobid = $scope.alljobs[i].id;

				distance = getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2);

				$scope.alljobs[i].distance = distance;

				//console.log("Distance between job: "+$scope.items[i].id+" and job: "+jobdata.id+" = "+ distance);

				neardistancejob.push($scope.alljobs[i]);
			}

			neardistancejob.sort(function(a, b) {
				return parseFloat(a.distance) - parseFloat(b.distance);
			});
			
			

			$timeout(function () {
				$scope.updateMapItems(neardistancejob);
			});
			

			/*$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			$state.go("app.singlejob",{jobId: marker.get("id")});*/
		}); 
     }

	$scope.goJob=function(item){
		$state.go("app.singlejob",{jobId: item.id}, {reload: true});
	}

	$scope.updateMapItems = function(itemsarray){
		
		var newitems = [];
		for (i = 0; i < itemsarray.length; i++){
			if(i<=4){
				newitems.push(itemsarray[i]);
			}
		}

		$scope.items = newitems;
	}
	
	$scope.showsearchmap = false;

	$scope.showSearchBar = function(){
		if($scope.showsearchmap){
			$scope.showsearchmap = false;
		} else {
			$scope.showsearchmap = true;
		}
	}
	
	$scope.search_seeker = "";

	$scope.searchJobsNew = function(keyword, category){
		$scope.keyword = keyword;
		initialize($scope.lat, $scope.log, category);
	}

	$scope.jobcategories = [];

	dbservice.getJobCategories().then(function(response){
		var downindarray = [];

		if(response != -1){

			var inddata =(response.data.data);

			for (var j in inddata){
				downindarray.push(inddata[j]);
			} 
		   
			$scope.jobcategories = downindarray;
			
			

			$timeout(function(){
				$scope.category = "0";
			}, 1000);

		}

	}); 



});