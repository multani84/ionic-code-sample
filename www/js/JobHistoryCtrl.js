angular.module('starter.controllers').controller('JobHistoryCtrl', function($scope,$rootScope, $stateParams, $state, userdetails, dbservice, $ionicLoading, $ionicPopup, $ionicScrollDelegate) {
	
	var user_id = userdetails.getUserId();  
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}

	$scope.loadingTrackers	=	true;
	$scope.appliedtab		=	"active";
	$scope.hiredtab			=	"";
	$scope.completedtab		=	"";
	
	$scope.noresulsapplied		= false;
	$scope.noresulscompleted	= false;
	$scope.noresulshired		= false;
	$scope.noresulsarchived		= false;

	var appliedarray = [];
	var hiredarray = [];
	var completedarray = [];
	var archivedarray = [];

	$scope.currentImage = 0;

	$scope.listCanSwipe = true;

	$scope.appliedjobsdiv = true;
	$scope.completedjobsdiv = false;
	$scope.hiredjobsdiv = false;
	
	var api_token = userdetails.getTokenId();

	dbservice.getJobHistory(user_id, api_token).then(function(response){
		var resdata = response.data;
		$scope.loadingTrackers = false;
		if(resdata.ResponseCode == true){
			if(resdata.data.length >0){
				for (var i in resdata.data){

					var archivejob = resdata.data[i].jobs.archivejobs_applied;
					
					if(resdata.data[i].status == "1" && resdata.data[i].jobs.status=="3"){ //Hired & completed
						resdata.data[i].jobs.jobstatus = "Completed";
					} else if(resdata.data[i].status == "1"){ // Hired But not completed
						resdata.data[i].jobs.jobstatus = "Hired";	
					} else { // Applied Not Hired
						resdata.data[i].jobs.jobstatus = "Applied";
					}

					if(archivejob.length === 0){
						if(resdata.data[i].status == "1" && resdata.data[i].jobs.status=="3"){ //completed
							completedarray.push(resdata.data[i].jobs);
						} else if(resdata.data[i].status == "1"){
							hiredarray.push(resdata.data[i].jobs);
						} else {
							appliedarray.push(resdata.data[i].jobs);
						}
					} else {
						archivedarray.push(resdata.data[i].jobs);
					}

					
				}

				$scope.appliedarray		= appliedarray;
				$scope.completedarray	= completedarray;
				$scope.hiredarray		= hiredarray;
				$scope.archivedarray	= archivedarray;

				console.log(archivedarray);

				if(appliedarray.length > 0){
					$scope.noresulsapplied		= false;
				} else {
					$scope.noresulsapplied		= true;
				}

				if(completedarray.length > 0){
					$scope.noresulscompleted	= false;
				} else {
					$scope.noresulscompleted		= true;
				}

				if(hiredarray.length > 0){
					$scope.noresulshired		= false;
				} else {
					$scope.noresulshired		= true;
				}

				if(archivedarray.length > 0){
					$scope.noresulsarchived		= false;
				} else {
					$scope.noresulsarchived		= true;
				}


			} else {
				$scope.appliedarray = $scope.completedarray = $scope.hiredarray = $scope.archivedarray = [];
				$scope.noresulsapplied		= true;
				$scope.noresulscompleted	= true;
				$scope.noresulshired		= true;
				$scope.noresulsarchived		= true;
			}
		} else {
			$scope.items = [];
			$scope.noresulsapplied		= true;
			$scope.noresulscompleted	= true;
			$scope.noresulshired		= true;
			$scope.noresulsarchived		= true;
		}
	}); 
  
	$scope.goApplied=function(item){
		$state.go("app.singlejob", {jobId: item.id});
	} 
	
	$scope.showJobHistory=function(typedata){
		$ionicScrollDelegate.scrollTop();
		if(typedata == "completed"){
			$scope.appliedtab		=	"";
			$scope.hiredtab			=	"";
			$scope.archivedtab		=	"";
			$scope.completedtab		=	"active";

			$scope.appliedjobsdiv	= false;
			$scope.completedjobsdiv = true;
			$scope.hiredjobsdiv		= false;
			$scope.archivedjobsdiv		= false;
		} else if(typedata == "hired"){
			$scope.appliedtab		=	"";
			$scope.hiredtab			=	"active";
			$scope.archivedtab		=	"";
			$scope.completedtab		=	"";

			$scope.appliedjobsdiv	= false;
			$scope.completedjobsdiv = false;
			$scope.hiredjobsdiv		= true;
			$scope.archivedjobsdiv		= false;
		} else if(typedata == "archived"){
			$scope.appliedtab		=	"";
			$scope.hiredtab			=	"";
			$scope.archivedtab		=	"active";
			$scope.completedtab		=	"";

			$scope.appliedjobsdiv	= false;
			$scope.completedjobsdiv = false;
			$scope.hiredjobsdiv		= false;
			$scope.archivedjobsdiv	= true;
		} else {
			$scope.appliedtab		=	"active";
			$scope.archivedtab		=	"";
			$scope.hiredtab			=	"";
			$scope.completedtab		=	"";

			$scope.appliedjobsdiv	= true;
			$scope.completedjobsdiv = false;
			$scope.hiredjobsdiv		= false;
			$scope.archivedjobsdiv		= false;
		}
	}  
	
	$scope.archiveJob = function(item, index){
		
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		var archive_type = 2; // 1 for all jobs , 2 for Applied Jobs

		dbservice.archiveJob(user_id, item.id, archive_type, api_token).then(function(response){
			$ionicLoading.hide();

			if(item.jobstatus == "Hired"){
				$scope.hiredarray.splice(index, 1);
				if($scope.hiredarray.length <=0){
					$scope.noresulshired = true;
				}
			} else if(item.jobstatus == "Completed"){
				$scope.completedarray.splice(index, 1);
				if($scope.completedarray.length <=0){
					$scope.noresulscompleted = true;
				}
			} else {
				$scope.appliedarray.splice(index, 1);
				if($scope.appliedarray.length <=0){
					$scope.noresulsapplied = true;
				}
			}

			$scope.archivedarray.push(item);

			$scope.noresulsarchived = false;

		});

		return true;
	}

	$scope.removeJob = function(item, index){

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		
		dbservice.removeAppliedJob(item.id, user_id, api_token).then(function(response){
			
			$ionicLoading.hide();

			if(response != "-1"){
				if(response.ResponseCode == true){
					$scope.appliedarray.splice(index, 1);
				} else {
					$ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: response.Message
					});
				}
			} else {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				});
			}
		});

	}


	$scope.unarchiveJob = function(item, index){
		
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		var archive_type = 2; // 1 for all jobs , 2 for Applied Jobs

		dbservice.unarchiveJob(user_id, item.id, archive_type, api_token).then(function(response){
			$ionicLoading.hide();
			$scope.archivedarray.splice(index, 1);


			if(item.jobstatus == "Hired"){
				$scope.hiredarray.push(item);
				$scope.noresulshired = false;
			} else if(item.jobstatus == "Completed"){
				$scope.completedarray.push(item);
				$scope.noresulscompleted = false;
			} else {
				$scope.appliedarray.push(item);
				$scope.noresulsapplied = false;
			}

			if($scope.archivedarray.length <=0){
				$scope.noresulsarchived = true;
			}

		});

		return true;
	}
	
	
})