angular.module('starter.controllers').controller('AppliedJobsCtrl', function($scope, $ionicLoading, $state, $stateParams, $rootScope, userdetails, dbservice, $ionicPopup) {
    
	var listarray = [];

	$scope.loadingTrackers = true;
             
	var user_id = userdetails.getUserId();  
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}

	$scope.listCanSwipe = true;

	$scope.noresultsmessage = "You have not applied to any jobs yet.";
	
	$scope.keyword = "";
	
	var api_token = userdetails.getTokenId();

	$scope.getAppliedJobs = function(keyword){

		var listarray=[];
		
		dbservice.getAppliedJobs(user_id, keyword, api_token).then(function(response){
			
			$ionicLoading.hide();
	
			$scope.loadingTrackers = false;
			
			if(response == -1){
				$scope.noresults = true;
			} else {
				if(response.data.ResponseCode == true){ 
					var data = response.data.data;
					
					for (var i in data){
						
						data[i].jobs.removejob = false;
						if(data[i].jobs.status == "3" && data[i].status == 1 ){ // Completed & Hired

							if(data[i].jobs.dueamount >0){
								data[i].jobs.jobstatus = "item item-complex item-right-editable item-left-editable completed-unpaid";
								data[i].jobs.jobstatusstr = "Completed";
							} else {
								data[i].jobs.jobstatus = "item item-complex item-right-editable item-left-editable completed-paid";
								data[i].jobs.jobstatusstr = "Completed";
							}

						} else if(data[i].jobs.status == 2 && data[i].status == 1){ // Hired
							data[i].jobs.jobstatus = "item item-complex item-right-editable item-left-editable inprogress";
							data[i].jobs.jobstatusstr = "Inprogress";
						} else {
							data[i].jobs.jobstatus = "item item-complex item-right-editable item-left-editable";
							data[i].jobs.jobstatusstr = "Applied";
							data[i].jobs.removejob = true;
						}
						
						var archivejob = data[i].jobs.archivejobs_applied;

						if(archivejob.length === 0){
							listarray.push(data[i].jobs);
						}

						
					}

					if(listarray.length == 0){
						$scope.noresults = true;
					} else {
						$scope.noresults = false;
					}

					$scope.items = listarray;

				} else if(response.ResponseCode== false){
					$scope.noresults = true;
				}
			}
		}); 

		
	}
          
	$scope.goSeeJob=function(item){
		$state.go("app.singlejob", {jobId: item.id}, {reload: true});
	}

	$scope.removeJob = function(item, index){

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		
		dbservice.removeAppliedJob(item.id, user_id, api_token).then(function(response){

			$ionicLoading.hide();

			if(response != "-1"){
				if(response.ResponseCode == true){
					$scope.items.splice(index, 1);
				} else {
					$ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: response.Message
					});
				}
			} else {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				});
			}
		});

	}


	$scope.SearchAppliedChange=function(keyword){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		$scope.getAppliedJobs(keyword);

	} 

	$scope.archiveJob = function(item, index){
		
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		var archive_type = 2; // 1 for all jobs , 2 for Applied Jobs

		dbservice.archiveJob(user_id, item.id, archive_type, api_token).then(function(response){
			$ionicLoading.hide();
			$scope.items.splice(index, 1);
			
		});

		return true;
	}

	$scope.getAppliedJobs("");
})