angular.module('starter.controllers').controller('LoginCtrl', function($scope, ngFB, $ionicPopup, $ionicModal ,$ionicNavBarDelegate, $cordovaDevice, $ionicLoading, $stateParams, $state, dbservice, $ionicHistory, $ionicPopup, localStorageService, $rootScope, $cordovaDevice, $cordovaPushV5, $http) {

	$scope.$parent.bottomtabs = false;

	var devicetype = "3"; // android 1 = IOS, 

	document.addEventListener("deviceready", function () {
		if($cordovaDevice.getPlatform().toLowerCase() === "ios"){
			devicetype = "1";
		}  else if($cordovaDevice.getPlatform().toLowerCase() === "android"){
			devicetype = "0";
		} else if($cordovaDevice.getPlatform().toLowerCase() === "BlackBerry 10"){
			devicetype = "2";
		} else {
			devicetype = "3";
		}

		var googleconfig = localStorageService.get("googleconfig");

		var device_id = localStorageService.get("deviceToken");

		if(device_id === null){
			if(devicetype != "3"){
				$cordovaPushV5.initialize(googleconfig).then(function(result) {
					$cordovaPushV5.onNotification();
					$cordovaPushV5.onError();
					$cordovaPushV5.register().then(function(registrationID) {
						device_id = registrationID;
						localStorageService.set("deviceToken", device_id);
					}, function(err) {
						/*$ionicPopup.alert({
							template: err
						});*/
					});
				}, function(err) {
					/*$ionicPopup.alert({
						template: err
					});*/
				});
			}
		}

	});



	$scope.LogIn=function(user){

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		
		var device_id = localStorageService.get("deviceToken");

		if(device_id === null){
			device_id = "0";
		} 
		
		dbservice.login(user.email, user.password, device_id, devicetype).then(function(response){
			
			if(response != -1){
				$ionicLoading.hide();

				if(response.data.ResponseCode == true){

					
					
					var udetails = response.data['data'];

					udetails.profile_picture = response.data.base_url + "" + udetails.profile_picture;

					//$http.defaults.headers.common.Authorization = udetails.api_token; 

					//$http.defaults.headers.common['Authorization'] = "Bearer-" + udetails.api_token;
					
					localStorageService.remove('userdetails');

					localStorageService.set("userdetails", udetails);

					if(udetails.user_job_role == 1){
						$scope.$parent.isjobseeker = false;
					} else {
						$scope.$parent.isjobseeker = true;
						$scope.$parent.isjobseekerhistory = true;
					}

					$ionicHistory.nextViewOptions({
						disableBack: true
					});

					var jobid = localStorageService.get("job_id_redirect");

					$rootScope.bottomtabs = true;
					$rootScope.showmenubutton = true;
					$scope.$parent.showloginicon = true;

					$scope.$emit('userlogin');

					if(jobid != null){
						$state.go("app.singlejob", {jobId: jobid}, {reload: true});
					}else{
						$state.go("app.map", null, {reload: true});
					}
				} else{
					$ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: response.data.Message
					});
				}
			} else {
				$ionicLoading.hide();
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				});
			}

		});
	}

	$ionicModal.fromTemplateUrl('templates/forgot-password.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});

	$scope.closeModal = function() {
		$scope.modal.hide();
	};

	$scope.forgotpwd=function(){
		$scope.modal.show();
	};

	$scope.sendforgotpassword = function(forgot){

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		localStorageService.set("verifymail", forgot.forgotemail);
		
		dbservice.forgotpassword(forgot.forgotemail).then(function(response){
			if(response != -1){
				$ionicLoading.hide();

				if(response.data.ResponseCode==true){
					$scope.modal.hide();
					$state.go("app.forgotpwd");
				} else {
					$ionicPopup.alert({
						template: response.data.Message
					});
				}
			} else {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				});
			}
		});


	};


	$scope.fbLogin = function () {
	
		ngFB.login({scope: 'email,read_stream,publish_actions'}).then(
			function(response) {
				if (response.status === 'connected') {
					ngFB.api({
						path: '/me',
						params: {fields: 'id,name,email,picture,first_name,last_name,gender'}
					}).then(
						function (user) {
							$scope.user = user;

							$ionicLoading.show({
								content: 'Loading',
								animation: 'fade-in',
								showBackdrop: true,
								maxWidth: 200,
								showDelay: 0
							});

							dbservice.fbloginCheck(user.id).then(function(response){
								
								$ionicLoading.hide();

								if(response != -1){

									if(response.data.ResponseCode == true){
										
										var udetails = response.data['data'];

										udetails.profile_picture = response.data.base_url + "" + udetails.profile_picture;
										
										localStorageService.remove('userdetails');

										localStorageService.set("userdetails", udetails);

										if(udetails.user_job_role == 1){
											$scope.$parent.isjobseeker = false;
										} else {
											$scope.$parent.isjobseeker = true;
										}


										$ionicHistory.nextViewOptions({
											disableBack: true
										});

										var jobid = localStorageService.get("job_id_redirect");

										$rootScope.bottomtabs = true;
										$rootScope.showmenubutton = true;
										$scope.$parent.showloginicon = true;

										$scope.$emit('userlogin');

										if(jobid != null){
											$state.go("app.singlejob", {jobId: jobid}, {reload: true});
										}else{
											$state.go("app.map");
										}
									} else {

										$scope.data = {}

										var useremail = $scope.user.email;
										
										if(useremail == "" || typeof useremail == 'undefined'){
											var myPopup = $ionicPopup.show({
												template: ' Enter email<input type="text" ng-model="data.userPassword">',
												title: 'Enter email',
												class:'required',
												subTitle: 'Please use normal things',
												scope: $scope,
												buttons: [{
													text: 'Cancel'
												}, {
													text: '<b>Save</b>',
													type: 'button-positive',
													onTap: function(e) {
														if (!$scope.data.userPassword) {
															e.preventDefault();
														} else {
															return $scope.data;
														}
													}
												}, ]

											});

											myPopup.then(function(res) {
												if (res) {
													var parts = JSON.stringify(res).split(':', 2);
													var the_text = parts[0];
													var the_num  = parts[1];
													// alert(the_num);
													var parts = the_num.split('"', 2);
													var idk = parts[0];
													var tdk_k  = parts[1];
													
													localStorageService.set("emailfb", tdk_k);
													
													$ionicLoading.show({
														content: 'Loading',
														animation: 'fade-in',
														showBackdrop: true,
														maxWidth: 200,
														showDelay: 0
													});

													if(localStorageService.get("emailfb")!=null) {  
														
														var device_id = localStorageService.get("deviceToken");

														if(device_id === null){
															device_id = 0;
														} 

														var pictureprofile = $scope.user.picture.data.url;
														
														dbservice.fblogin(localStorageService.get("emailfb"), $scope.user.id, device_id, $scope.user.name, $scope.user.first_name, $scope.user.last_name, pictureprofile, $scope.user.gender, devicetype).then(function(response){
														
															$ionicLoading.hide();
														
															if(response.data.ResponseCode == true){

																var udetails = response.data['data'];

																udetails.profile_picture = response.data.base_url + "" + udetails.profile_picture;
																
																localStorageService.remove('userdetails');

																localStorageService.set("userdetails", udetails);

																$ionicHistory.nextViewOptions({
																	disableBack: true
																});

																var jobid = localStorageService.get("job_id_redirect");

																$scope.$parent.bottomtabs = true;
																$scope.$parent.showmenubutton = true;
																$scope.$parent.showloginicon = true;

																if(jobid != null){
																	$state.go("app.singlejob", {jobId: jobid}, {reload: true});
																}else{
																	$state.go("app.map");
																}
																
															}else{
																$ionicPopup.alert({
																	cssClass: 'popup-no-title',
																	template: "Due to some technical issues we are unable to process your request. Please try again later."
																});
															}
								  
														});
								 
													}
												} else {
													$ionicPopup.alert({
														cssClass: 'popup-no-title',
														template: "Please provide your email address for updates."
													});
												}
											});
										} else {
											var device_id = localStorageService.get("deviceToken");

											if(device_id === null){
												device_id = 0;
											} 

											var pictureprofile = $scope.user.picture.data.url;
											
											dbservice.fblogin($scope.user.email, $scope.user.id, device_id, $scope.user.name, $scope.user.first_name, $scope.user.last_name, pictureprofile, $scope.user.gender, devicetype).then(function(response){
											
												$ionicLoading.hide();
											
												if(response.data.ResponseCode == true){

													var udetails = response.data['data'];

													udetails.profile_picture = response.data.base_url + "" + udetails.profile_picture;
													
													localStorageService.remove('userdetails');

													localStorageService.set("userdetails", udetails);

													$ionicHistory.nextViewOptions({
														disableBack: true
													});

													var jobid = localStorageService.get("job_id_redirect");

													$scope.$parent.bottomtabs = true;
													$scope.$parent.showmenubutton = true;
													$scope.$parent.showloginicon = true;

													if(jobid != null){
														$state.go("app.singlejob", {jobId: jobid}, {reload: true});
													}else{
														$state.go("app.map");
													}
													
												}else{
													$ionicPopup.alert({
														cssClass: 'popup-no-title',
														template: "Due to some technical issues we are unable to process your request. Please try again later."
													});
												}
					  
											});
										}
									}
								}   
							});
						},
						function (error) {
							$ionicPopup.alert({
								template: error.error_description
							});
						}
					);

					
					
					
				} else {
					$ionicPopup.alert({
						template: 'Facebook login failed'
					});
				}
			});
	}

	$scope.signup=function(){
		$state.go("app.signup", null, {reload: true});
	}

});