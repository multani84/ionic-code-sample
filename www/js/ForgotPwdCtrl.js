angular.module('starter.controllers').controller('ForgotPwdCtrl', function($scope, $http, $state, myURL, $ionicLoading, $ionicPopup, localStorageService) {
	$scope.Verifypwdcode=function(Verifypwdcode){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});


		var link=myURL.sayURL+'/user/verifyforgotpasswordcode';

		var emailaddress = localStorageService.get("verifymail");

		$http.post(link, {email : emailaddress, code: Verifypwdcode.code}).then(function successCallback(response){
			$ionicLoading.hide();
			if(response.data.ResponseCode==true){
				localStorageService.set("user_id", response.data.data.id);
				$state.go("app.changepassword");
			} else {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: response.data.Message
				});
			}
		}, function errorCallback(response) {
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "Due to some technical issues we are unable to process your request. Please try again later."
			});
		});
         
	}
})