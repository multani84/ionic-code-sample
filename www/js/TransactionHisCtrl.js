angular.module('starter.controllers').controller('TransactionHisCtrl', function($scope, $ionicLoading, $state, dbservice, userdetails) {
	
	var user_id = userdetails.getUserId();  
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}

	$scope.loadingTrackers = true;

	var listarray=[];
	
	var api_token = userdetails.getTokenId();

    dbservice.getTransactionHistory(user_id, api_token).then(function(response){
		
		$scope.loadingTrackers = false;

		if(response.ResponseCode== true){ 
			
			var data = response.data;

			for (var i in data){
				if(data[i].touser_id == user_id){ // Credit
					data[i].amount = parseFloat(data[i].amount_paid);
					data[i].type = "Credit";
					data[i].typecolor = "balanced";
				} else { // Credit
					data[i].amount = parseFloat(data[i].amount_paid) + parseFloat(data[i].commission);
					data[i].type = "Debit";
					data[i].typecolor = "assertive";
				}

				console.log(data[i].amount);
				listarray.push(data[i]);
			}
			$scope.transations = listarray;

			if(listarray.length>0){
				$scope.nohis=false;
				$scope.showhis=true;
			} else {
				$scope.showhis=false;
				$scope.nohis=true;
			}

		} else if(response.ResponseCode== false){  
			$state.go("app.map");   
		}

	 });

	 $scope.goToJob=function(jobid){
		$state.go("app.singlejob", {jobId: jobid});
	} 
})