// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngCordova', 'ngOpenFB', 'LocalStorageModule','ionic-datepicker'])

.run(function($ionicPlatform, ngFB, $cordovaDevice, $cordovaPushV5, $ionicPopup, $ionicLoading, $http, $rootScope, localStorageService, $state, $cordovaStatusbar, $ionicHistory, $cordovaNetwork, dbservice) {

  $ionicPlatform.ready(function() {

	$rootScope.appname    = ""; //Hundelufteren
	$rootScope.commission = 0;
	$rootScope.term_link = "#";
	$rootScope.privacy_link = "#";
	$rootScope.mincommission = 0;
	$rootScope.create_job_msg = "";

	setTimeout(function() {
        navigator.splashscreen.hide();
    }, 3000);

	dbservice.getSettings().then(function(response){
		if(response.ResponseCode == true){
			var configuration = response.data;
			for (var j in configuration){
				if(configuration[j].meta_key == "app_name"){
					$rootScope.appname = configuration[j].meta_value;
				} else if(configuration[j].meta_key == "admin_commission"){
					$rootScope.commission = configuration[j].meta_value;
				} else if(configuration[j].meta_key == "term_link"){
					$rootScope.term_link = configuration[j].meta_value;
				} else if(configuration[j].meta_key == "privacy_link"){
					$rootScope.privacy_link = configuration[j].meta_value;
				} else if(configuration[j].meta_key == "minimum_commission"){
					$rootScope.mincommission = configuration[j].meta_value;
				}else if(configuration[j].meta_key == "create_job_msg"){
					$rootScope.create_job_msg = configuration[j].meta_value;
				}
			} 
		}

		$rootScope.$broadcast('settingsintialized');
	});
	
	$cordovaStatusbar.style(1);

	// supported names: black, darkGray, lightGray, white, gray, red, green,
	// blue, cyan, yellow, magenta, orange, purple, brown
	$cordovaStatusbar.styleColor('white');

	$cordovaStatusbar.styleHex('#1aa6c9');

	ngFB.init({appId: '1896405820646329'});

	var deviceToken = "";

	config =  {
		android: {
			senderID: "882011453356",
			alert: 'true',
			badge: 'true',
			sound: 'true',
			clearBadge: true
		},
		ios: {
			alert: 'true',
			badge: 'true',
			sound: 'true',
			clearBadge: true
		}
	};

	localStorageService.set("googleconfig", config);

	$cordovaPushV5.initialize(config).then(function(result) {
		
		$cordovaPushV5.onNotification();
		$cordovaPushV5.onError();
		$cordovaPushV5.register().then(function(registrationID) {
			deviceToken = registrationID;
			localStorageService.set("deviceToken",deviceToken);
			
		}, function(err) {
			$ionicPopup.alert({
				template: err
			});
		});
	}, function(err) {
		/*$ionicPopup.alert({
			template: err
		});*/
	});

	$rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {  // use two variables here, event and data !!!
		

		if (data.additionalData.foreground === true) {
			// do something if the app is in foreground while receiving to push - handle in app push handling
			
			if(device.platform.toLowerCase() === "android"){
				var media = new Media("/android_asset/www/css/beep.ogg");
			} else {
				var media = new Media("css/beep.ogg");
			}

			media.play({ playAudioWhenScreenIsLocked : false });
			
			var confirmPopup = $ionicPopup.confirm({
				title: data.additionalData.subtitle,
				template: data.message,
				cancelText: 'Not Now',
				okText: 'View',
				cancelType: 'button-assertive', // String (default: 'button-default'). The type of the Cancel button.
				okType: 'button-balanced', // String (default: 'button-positive'). The type of the OK button.
			});

			confirmPopup.then(function(res) {
				if(res) {
					if(typeof data.additionalData.statename != 'undefined'){

						if(data.additionalData.statename == "app.singlemypost"){
							$state.go(data.additionalData.statename, {jobId: data.additionalData.jobid}, {reload: true}); 
						} else if(data.additionalData.statename == "app.singlejob"){
							$state.go(data.additionalData.statename, {jobId: data.additionalData.jobid}, {reload: true}); 
						} else {
							$state.go(data.additionalData.statename, null, {reload: true}); 
						}
					}
				} else {
					//continue
				}
			});

		} else {
		   // handle push messages while app is in background or not started

		   if(typeof data.additionalData.statename != 'undefined'){
				if(data.additionalData.statename == "app.singlemypost"){
					$state.go(data.additionalData.statename, {jobId: data.additionalData.jobid}, {reload: true}); 
				} else if(data.additionalData.statename == "app.singlejob"){
					$state.go(data.additionalData.statename, {jobId: data.additionalData.jobid}, {reload: true}); 
				} else {
					$state.go(data.additionalData.statename, null, {reload: true}); 
				}
			}

		}
		if (Device.isOniOS()) {
			if (data.additionalData.badge) {
				$cordovaPushV5.setBadgeNumber(NewNumber).then(function (result) {
					// OK
				}, function (err) {
					// handle error
				});
			}
		}

		$cordovaPushV5.finish().then(function (result) {
			// OK finished - works only with the dev-next version of pushV5.js in ngCordova as of February 8, 2016
		}, function (err) {
			// handle error
		});
	});

	$rootScope.$on('$cordovaPushV5:errorOccurred', function(event, error) {
		// handle error
		/*$ionicPopup.alert({
			template: "Please check your internet connection"
		});*/
	});

		$rootScope.nointernet = 0;

		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
		  cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		  cordova.plugins.Keyboard.disableScroll(true);

		}

		if (window.StatusBar) {
		  // org.apache.cordova.statusbar required
		  StatusBar.styleDefault();
		}

		$rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
			if($rootScope.nointernet == 0){
				$rootScope.nointernet = 1;
				var nointernet = $ionicPopup.alert({
					title: 'Internet Disconnected',
					content: "Sorry, no Internet connectivity detected. Please reconnect and try again."
				});

				nointernet.then(function(res) {
					$rootScope.nointernet = 0;
				});
			}
		});

		var isOffline = $cordovaNetwork.isOffline();

		if(isOffline){
			if($rootScope.nointernet == 0){
				$rootScope.nointernet = 1;
				var nointernet = $ionicPopup.alert({
					title: 'Internet Disconnected',
					content: "Sorry, no Internet connectivity detected. Please reconnect and try again."
				});

				nointernet.then(function(res) {
					$rootScope.nointernet = 0;
				});
			}
		}


		$rootScope.$on('$cordovaNetwork:online', function(event, networkState){
			$state.go($state.current.name, null, {reload: true});
		});


	});
})

.config(function($stateProvider, $ionicConfigProvider, $urlRouterProvider, localStorageServiceProvider, ionicDatePickerProvider) {

	var datePickerObj = {
      inputDate: new Date(),
      titleLabel: 'Select a Date',
      setLabel: 'Set',
      todayLabel: 'Today',
      closeLabel: 'Close',
      mondayFirst: false,
      weeksList: ["S", "M", "T", "W", "T", "F", "S"],
      monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
      templateType: 'popup',
      from: new Date(2012, 8, 1),
      to: new Date(2018, 8, 1),
      showTodayButton: false,
      dateFormat: 'dd MM yyyy',
      closeOnSelect: true,
      disableWeekdays: []
    };
    ionicDatePickerProvider.configDatePicker(datePickerObj);
	
	$ionicConfigProvider.backButton.text('');
	$ionicConfigProvider.backButton.previousTitleText(false);
	
	localStorageServiceProvider
    .setStorageType('localStorage')
    .setNotify(true, true);

	$stateProvider

	.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/menu.html',
		controller: 'AppCtrl'
	})
	.state('app.map', {
		url: '/map',
		cache: false,
		views: {
			'menuContent': {
				templateUrl: 'templates/map.html',
				controller: 'MapCtrl'
			}
		}
	})

	.state('app.nointernet', {
      url: '/nointernet',
      views: {
        'menuContent': {
          templateUrl: 'templates/no-internet.html',
          controller: 'NointernetCtrl'
        }
      }
    })

	.state('app.singlejob', {
		url: '/job/:jobId',
		cache: false,
		views: {
			'menuContent': {
				templateUrl: 'templates/jobsingle.html',
				controller: 'SingleJobCtrl'
			}
		}
	})

	.state('app.login', {
      url: '/login',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })

	.state('app.forgotpwd', {
      url: '/forgotpwd',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/forgotpwd.html',
          controller: 'ForgotPwdCtrl'
        }
      }
    })

	.state('app.signup', {
      url: '/signup',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/signup.html',
          controller: 'SignupCtrl'
        }
      }
    })

	.state('app.changepassword', {
      url: '/changepassword',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/changepassword.html',
          controller: 'ChangePwdCtrl'
        }
      }
    })

	.state('app.appliedjobs', { // Jobs Applied By Me
      url: '/myjobs',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/myjobs.html',
          controller: 'AppliedJobsCtrl'
        }
      }
    })

	.state('app.jobs', {	// All Jobs
      url: '/jobs',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/jobs.html',
          controller: 'JobCtrl'
        }
      }
    })

	.state('app.myjobs', { // Jobs posted By Me
      url: '/myposts',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/myposts.html',
          controller: 'MyJobsCtrl'
        }
      }
    })
	
	.state('app.createjob', {
      url: '/createjob',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/createjob.html',
          controller: 'CreateJobCtrl'
        }
      }
    })

	.state('app.createjobseeker', {
      url: '/createjob/:seekerid',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/createjob.html',
          controller: 'CreateJobCtrl'
        }
      }
    })

	.state('app.singlemypost', {
		url: '/mypost/:jobId',
		cache: false,
		views: {
			'menuContent': {
				templateUrl: 'templates/mypostsingle.html',
				controller: 'SinglePostCtrl'
			}
		}
	})

	.state('app.profile', {
		url: '/profile/:profileid',
		views: {
			'menuContent': {
				templateUrl: 'templates/viewprofile.html',
				controller: 'ViewProfileCtrl'
			}
		}
	})

	.state('app.profilejob', {
		url: '/profile/:profileid/:jobid',
		cache: false,
		views: {
			'menuContent': {
				templateUrl: 'templates/viewprofile.html',
				controller: 'ViewProfileCtrl'
			}
		}
	})

	.state('app.accounts', {
      url: '/accounts',
      views: {
        'menuContent': {
          templateUrl: 'templates/accounts.html',
          controller: 'AccountCtrl'
        }
      }
    })
	
	.state('app.seeker', {
      url: '/seeker',
	 cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/seeker.html',
          controller: 'SeekerCtrl'
        }
      }
    })

	.state('app.jobhistory', {
      url: '/jobhistory',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/jobhistory.html',
          controller: 'JobHistoryCtrl'
        }
      }
    })

	.state('app.transactionhistory', {
      url: '/transactionhistory',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/transactionhistory.html',
          controller: 'TransactionHisCtrl'
        }
      }
    })
	
	 .state('app.payment_details', {
      url: '/payment_details',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/payment_details.html',
          controller: 'PayDetailsCtrl'
        }
      }
    })

	.state('app.changepwd', {
      url: '/changepwd',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/changepwdprofile.html',
          controller: 'ChangePasswordCtrl'
        }
      }
    })

	.state('app.update_profile', {
	  cache: false,
      url: '/update_profile',
      views: {
        'menuContent': {
          templateUrl: 'templates/update_profile.html',
          controller: 'UpdateProfileCtrl'
        }
      }
    })

	.state('app.feedback', {
      url: '/feedback/:uid/:jobid',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/ratings.html',
          controller: 'FeedbackCtrl'
        }
      }
    })

	.state('app.pay_rate', {
     url: '/pay_rate/:dueAmount/:jobid/:profileid',
     cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/pay_rate.html',
          controller: 'PayRateCtrl'
        }
      }
    })

	 .state('app.chatlist', {
      url: '/chatlist',
	  cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/chatlist.html',
          controller: 'ChatListCtrl'
        }
      }
    })
	 .state('app.chat', {
      url: '/chat/:id/:jobid',
	cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/chat.html',
          controller: 'ChatCtrl'
        }
      }
    })

	.state('app.paypal', {
      url: '/paypal',
      views: {
        'menuContent': {
          templateUrl: 'templates/paypal.html',
          controller: 'PaypalCtrl'
        }
      }
    })

	.state('app.updatejob', {
	  cache: false,
      url: '/updatejob/:jobidupdate',
      views: {
        'menuContent': {
          templateUrl: 'templates/updateJob.html',
          controller: 'UpdateJobCtrl'
        }
      }
    })

	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/app/map');
});
