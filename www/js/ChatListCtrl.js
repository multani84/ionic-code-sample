angular.module('starter.controllers').controller('ChatListCtrl', function($state, $scope, dbservice, $ionicLoading, userdetails) {

	var user_id = userdetails.getUserId();  
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$state.go("app.login");
	}
	
	$scope.sender_id = user_id;

	$scope.loadingchats = true;

	var api_token = userdetails.getTokenId();

	$scope.imgdomainurl = dbservice.domainurl + 'public/images/small/';

	var init = function(){

		$scope.messages = [];
	
		dbservice.getAllMessages($scope.sender_id, api_token).then(function(response){
			
			$scope.loadingchats = false;

			if(response.ResponseCode == true){
				
				if(response.data.length <=0){
					$scope.noresults = true;
				} else {
					$scope.noresults = false;
				}
				for (var i in response.data){
					var todaydate = new Date();
					var newdate = new Date(response.data[i].date);

					if(todaydate.toDateString() == newdate.toDateString()){
						
						var hours = newdate.getHours();
						var minutes = newdate.getMinutes();
						var ampm = hours >= 12 ? 'pm' : 'am';
						hours = hours % 12;
						hours = hours ? hours : 12; // the hour '0' should be '12'
						minutes = minutes < 10 ? '0'+minutes : minutes;
						var strTime = hours + ':' + minutes + ' ' + ampm;

						response.data[i].datenow = strTime;
					} else {
						var hours = newdate.getHours();
						var minutes = newdate.getMinutes();
						var ampm = hours >= 12 ? 'pm' : 'am';
						hours = hours % 12;
						hours = hours ? hours : 12; // the hour '0' should be '12'
						minutes = minutes < 10 ? '0'+minutes : minutes;

						var datestr = newdate.getDate();
						datestr = datestr < 10 ? '0'+datestr : datestr;
						var monthstr = newdate.getMonth()+1;
						monthstr = monthstr < 10 ? '0'+monthstr : monthstr;

						var strTime = datestr + "/" + monthstr + "/" + newdate.getFullYear() + " " + hours + ':' + minutes + ' ' + ampm;
						
						response.data[i].datenow = strTime;
					}

					response.data[i].count = 0;

				
				}

				$scope.messages = response.data;

			} else {
				$scope.noresults = false;
			}

		}).finally(function() {
		// Stop the ion-refresher from spinning
		$scope.$broadcast('scroll.refreshComplete');
		});
	};

	$scope.doRefresh = function() {
		init();
	};

	init();

	$scope.goToChat = function(message){
		if(message.receiver_id == user_id){
			$state.go("app.chat", {id: message.sender_id, jobid: message.job_id}, {reload: true});
		} else {
			$state.go("app.chat", {id: message.receiver_id, jobid: message.job_id}, {reload: true});
		}
	};
	
	$scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {

		var api_token = userdetails.getTokenId();

		for (var i in $scope.messages){
			var sender_id = 0;

			if($scope.messages[i].receiver_id == user_id){
				sender_id = $scope.messages[i].sender_id;
			} else {
				sender_id = $scope.messages[i].receiver_id;
			}

			dbservice.getMessagesCountUsers(sender_id, user_id, $scope.messages[i].job_id, i, api_token).then(function(response){
				if(response.data >0){
					console.log(response);
					$scope.messages[response.indexid].count = response.data;
				}
			});
		}
    });
});