angular.module('starter.controllers').controller('UpdateJobCtrl', function($scope, $ionicLoading, $stateParams, $state, myURL, $http, userdetails, $ionicPopup, $cordovaDatePicker, ionicDatePicker, dbservice, $ionicActionSheet, camera, $ionicModal) {

      
    var downindarray=[];

	$scope.job = {job_title: "", sdate: "", edate:"", stime: "", etime: "", job_location: "", description: "", user_price: 0.00, newimage: "", oldimage: ""};
	
	$scope.uploadimagetxt = "New Image";

	var user_id = userdetails.getUserId();  
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$state.go("app.login");
	}

	var jobid = $stateParams.jobidupdate;

	$scope.showDatePicker = function(esdate){

		var ipObj1 = {
		  titleLabel: 'Select a Date',
		  callback: function (val) {  //Mandatory
			
			var newdate = new Date(val);

			var month = (newdate.getMonth() + 1);

			if(month < 10){
				month = "0" + month;
			}

			var dateselected = newdate.getDate();

			if(dateselected < 10){
				dateselected = "0" + dateselected;
			}

			var datestring = dateselected + '/' + month + '/' +  newdate.getFullYear();
			
			var aajdate = new Date();
			var aajmonth = (aajdate.getMonth() + 1);
			if(aajmonth < 10){
				aajmonth = "0" + aajmonth;
			}
			var aajdateselected = aajdate.getDate();

			if(aajdateselected < 10){
				aajdateselected = "0" + aajdateselected;
			}

			var today = new Date(aajmonth+"/"+aajdateselected+"/"+aajdate.getFullYear());
			
			if (newdate.getTime()<today.getTime()) {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Date can not be earlier than today."
				});
				return false;
			}


			if(esdate == 'edate'){
				$scope.job.edate = datestring;
			} else {
				$scope.job.sdate = datestring;
			}
		  }
		};

		ionicDatePicker.openDatePicker(ipObj1);

		/*var options = {
			date: new Date(),
			mode: 'date', // or 'time'
			minDate: new Date() - 10000,
			allowOldDates: true,
			allowFutureDates: false,
			doneButtonLabel: 'DONE',
			doneButtonColor: '#F2F3F4',
			cancelButtonLabel: 'CANCEL',
			cancelButtonColor: '#000000'
		};

		$cordovaDatePicker.show(options).then(function(date){

			var newdate = new Date(date);
			var datestring = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();

			if(esdate == 'edate'){
				$scope.job.edate = datestring;
			} else {
				$scope.job.sdate = datestring;
			}
			
		});*/
	}

	$scope.showTimePicker = function(esdate){
		var options = {
			date: new Date(),
			mode: 'time', // or 'time'
			minDate: new Date() - 10000,
			allowOldDates: true,
			allowFutureDates: false,
			doneButtonLabel: 'DONE',
			doneButtonColor: '#F2F3F4',
			cancelButtonLabel: 'CANCEL',
			cancelButtonColor: '#000000'
		};

		$cordovaDatePicker.show(options).then(function(time){

			var datetext = time.toTimeString();
			// datestring is "20:32:01 GMT+0530 (India Standard Time)"
			// Split with ' ' and we get: ["20:32:01", "GMT+0530", "(India", "Standard", "Time)"]
			// Take the first value from array :)
			datetext = datetext.split(' ')[0];

			if(esdate == 'etime'){
				$scope.job.etime = datetext;
			} else {
				$scope.job.stime = datetext;
			}
		});
	}


	$scope.collections = [
	  { name: 'Hourly', value: 'hourly' }, 
	  { name: 'Fixed', value: 'fixed' }
	];

	
	$scope.oldstartdate = "";
	$scope.oldenddate = "";

	dbservice.getJobDetails(jobid, user_id).then(function(response){
		if(response.ResponseCode == true){
			$scope.job = response.data;
			$scope.job.stime = response.data.start_time;
			$scope.job.etime = response.data.end_time;
			$scope.job.job_location = response.data.address;
			$scope.job.user_price = Number(response.data.cost);

			$scope.job.oldimage = response.data.image;

			$scope.job.billing_type_txt = "Select Billing Type";
			$scope.job.job_category_txt = "Select Job Category";
			
			if(response.data.image != ""){
				$scope.jobimage = true;
			} else {
				$scope.jobimage = false;
			}

			var startdate = response.data.start_date;

			var sdates = startdate.split("-");

			$scope.job.sdate = sdates[2] + "/"+ sdates[1] + "/" +sdates[0];

			$scope.oldstartdate = sdates[0]+"-"+sdates[1]+"-"+sdates[2];
			
			var enddate = response.data.end_date;

			var edates = enddate.split("-");

			$scope.oldenddate = edates[0]+"-"+edates[1]+"-"+edates[2];

			$scope.job.edate = edates[2] + "/"+ edates[1] + "/" +edates[0];

			var jobtype =  response.data.job_type;

			if(jobtype == "fixed"){
				$scope.job.selectedOption = $scope.collections[1];
			} else {
				$scope.job.selectedOption = $scope.collections[0];
			}

			if($scope.job.job_type = "hourly"){
				$scope.selectedcatindex = 0;
				$scope.job.billing_type_txt = "Hourly";
				$scope.job.billing_type = "hourly";
			} else {
				$scope.selectedcatindex = 1;
				$scope.job.billing_type_txt = "Fixed";
				$scope.job.billing_type = "fixed";
			}
			
			var urlind=myURL.sayURL+'/category/all';

			$http.get(urlind).success(function(responsecategories) {

				var inddata =(responsecategories.data);

				//downindarray.push({name: "Select Category", id: 0});

				for (var j in inddata){
					downindarray.push(inddata[j]);
				} 
			   
				$scope.jobcategories = downindarray;
				
				var cat_id = $scope.job.cat_id;

	
				$scope.job.job_category_txt = response.data.category.name;

				$scope.job.category = cat_id;

				$scope.selectedcatindex = cat_id;

				$ionicModal.fromTemplateUrl('templates/jobcategories.html', {
					scope: $scope,
					animation: 'slide-in-up'
				}).then(function(modal) {
					$scope.jobcategoriesmodal = modal;
				});


			}); 
	
			
			
		}
	});
   
	
	function getLatitudeLongitude(callback, address) {
		// If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
		address = address || 'Ferrol, Galicia, Spain';
		// Initialize the Geocoder
        geocoder = new google.maps.Geocoder();
        if (geocoder) {
            geocoder.geocode({
                'address': address
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    callback(results[0]);
                }else{
                  $scope.job_lat = 0;
                  $scope.job_lng = 0;
                }
            });
        }
	}


	$scope.updateJob=function(job){

		if(typeof job.category == 'undefined' || (job.category <= 0 || job.category == "")){
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "Please select job category."
			});
		} else {

			var startdate = job.sdate;

			var sdates = startdate.split("/");

			var startdate = sdates[2] + "-"+ sdates[1] + "-" +sdates[0];

			var enddate = job.edate;

			var edates = enddate.split("/");

			var enddate = edates[2] + "-"+ edates[1] + "-" +edates[0];
	  
			var address = job.job_location;
			
			var sdate = new Date(sdates[2] + "/"+ sdates[1] + "/" +sdates[0]);
			var edate = new Date(edates[2] + "/"+ edates[1] + "/" +edates[0]);

			var aajdate = new Date();
			var aajmonth = (aajdate.getMonth() + 1);
			if(aajmonth < 10){
				aajmonth = "0" + aajmonth;
			}
			var aajdateselected = aajdate.getDate();

			if(aajdateselected < 10){
				aajdateselected = "0" + aajdateselected;
			}

			var today = new Date(aajdate.getFullYear()+"/"+aajmonth+"/"+aajdateselected);


			var hourstime = Math.abs(Date.parse('01/01/2011 '+job.etime+'') - Date.parse('01/01/2011 '+job.stime+'')) / 36e5;

			//36e5 is the scientific notation for 60*60*1000

			var jobtype = job.billing_type;
			
			if(sdate.getTime() > edate.getTime()){
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Start date cannot be greater than end date."
				});
				return false;
			} else {
				if(jobtype != "fixed" && jobtype != "Fixed"){

					if(sdate.getTime() > edate.getTime()){
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "Start date cannot be greater than end date."
						});
						return false;
					}  else if((Date.parse('01/01/2011 '+job.stime+'') >= Date.parse('01/01/2011 '+job.etime+'')) || hourstime<1){
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "Start time should be less than end time. There should be difference of one hour atleast."
						});
						return false;
					} else {
						var oldstartdate = $scope.oldstartdate;
						if((startdate != oldstartdate) && (sdate.getTime() < today.getTime())){
							console.log("New Start Date: "+startdate+ " Old Start Date: " + $scope.oldstartdate);
							$ionicPopup.alert({
								cssClass: 'popup-no-title',
								template: "Start should be greater than today date."
							});
							return false;
						} else if((enddate != $scope.oldenddate) && (edate.getTime()<today.getTime())){
							$ionicPopup.alert({
								cssClass: 'popup-no-title',
								template: "End Date should be greater than today date."
							});
							return false;
						}
					}
				}
			}

			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
  
			var address = job.job_location;

			var fileTypes = ['jpg', 'jpeg', 'png', 'gif']; 
			 
			getLatitudeLongitude(function(result){
				if(typeof result.geometry != 'undefined'){
					$scope.job_lat = result.geometry.location.lat();
					$scope.job_lng = result.geometry.location.lng();
				} else {
					$scope.job_lat = 0;
					$scope.job_lng = 0;
				}

				user_id = userdetails.getUserId(); 

				var api_token = userdetails.getTokenId();

				var link=myURL.sayURL+'/jobs/updatejob?api_token='+api_token;
				
					
				var imgData = $scope.job.newimage;
				
				if(imgData != ""){
					var postdata = {job_id: jobid, user_id : user_id, cat_id : job.category, job_title : job.job_title, description : job.description, lat : $scope.job_lat, lng:$scope.job_lng, address:job.job_location, start_date: startdate, end_date: enddate, start_time: job.stime, end_time: job.etime, img:imgData, job_type: job.billing_type, cost: job.user_price};
				} else {
					var postdata = {job_id: jobid, user_id : user_id, cat_id : job.category, job_title : job.job_title, description : job.description, lat : $scope.job_lat, lng:$scope.job_lng, address:job.job_location, start_date: startdate, end_date: enddate, start_time: job.stime, end_time: job.etime, job_type: job.billing_type, cost: job.user_price};
				}
				

				$http.post(link, postdata).then(function successCallback(response) {
					$ionicLoading.hide();
					if(response.data.ResponseCode == true){
						var successjob = $ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "Your job has been updated successfully."
						});

						successjob.then(function(res) {
							$state.go("app.myjobs", null, {reload: true});
						});

					} else {
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: response.data.Message
						});
					}
				// $state.go("app.indicator");
				}, function errorCallback(response) {
					$ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: "Due to some technical issues we are unable to process your request. Please try again later."
					});
				});
				

			}, address);
		}
       
	}     


	$scope.removeImage = function(){
		$scope.uploadimagetxt = "New Image";
		$scope.job.image = $scope.job.oldimage;
		$scope.job.newimage = "";
		$scope.showimage = false;
		$scope.deleteimage = false;
	}


	$scope.openPopover = function() {
		// Show the action sheet
		var showActionSheet = $ionicActionSheet.show({
			buttons: [
			{ text: '<i class="ion-android-image"></i> Gallery',  },
			{ text: '<i class="ion-android-camera"></i> Camera' }
			],

			titleText: 'Choose Image',
			cancelText: 'Cancel',
			cssClass: 'uploadimage',
			cancel: function() {
			// add cancel code...
			},

			buttonClicked: function(index) {
				if(index === 0) { // Gallery
					var type = "gallery";
				} else if(index === 1){ // Camera
					var type = "camera";
				}
				camera.getPicture(type).then(function (imageData) {
					if(imageData != ""){
						var image = document.getElementById('updateImage');
						$scope.job.image = imageData;
						image.src = imageData;
						$scope.job.newimage = imageData
						$scope.showimage = true;
						$scope.uploadimagetxt = "Change Image";
						$scope.deleteimage = true;
					}					
				});

				return true;
			},

			destructiveButtonClicked: function() {
			// add delete code..
			}
		});
	};


	$scope.billingtype=function(){
		var showBillingType = $ionicPopup.show({
			templateUrl: 'templates/job_type.html',
			title: 'Choose Job Type',
			subTitle: '',
			scope: $scope,
			buttons: []
		});

		$scope.showBillingType = showBillingType;
	};

	$scope.jobtypeselected = function(billingtype){
		$scope.job.billing_type = billingtype.value;
		$scope.job.billing_type_txt = billingtype.name;
		$scope.showBillingType.close();
	};

	$scope.showjobcategories = function(){
		$scope.jobcategoriesmodal.show();
	};

	$scope.closeJobsCategoryModal = function(){
		$scope.jobcategoriesmodal.hide();
	};

	$scope.selectjobcategory = function(category, index){
		$scope.job.category = category.id;
		$scope.job.job_category_txt = category.name;
		$scope.jobcategoriesmodal.hide();
	};

})