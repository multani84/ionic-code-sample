angular.module('starter.controllers').controller('SignupCtrl', function($scope, $rootScope, $stateParams, $ionicLoading, $state, $ionicModal, myURL, $http, $ionicPopup, $ionicActionSheet, camera) {

	$scope.images = [];
	
	$scope.handleClick=function(cb) {
		if(cb.baaSelected){
			$scope.jobseeker = true;
		} else {
			$scope.jobseeker = false;
		}
	}

	$scope.deleteimage = false;
	$scope.showimage = false;
	$scope.image = "images/add-image.png";

	$scope.SignUp=function(signupdata){

		
         
		var fname		=	signupdata.firstname;
		var lname		=	signupdata.lastname;
		var email		=	signupdata.email;
		var password	=	signupdata.password;
		var cpassword	=	signupdata.cpassword;
		var phone		=	signupdata.contact;
		var price		=	signupdata.user_price;
		
		password		= password.trim();

		cpassword		= cpassword.trim();

		if(password == "") {
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "Password cannot be empty or contain white spaces only."
			});
			return false;
		}
		
		if(cpassword != password){
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "Confirm password should be same as password."
			});
			return false;
		}

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		var user_job_role=0;

		if(signupdata.baaSelected){
			var user_job_role	=	2;
		} else {
			var user_job_role	=	1;
			price = 0.00;
		}

		var link=myURL.sayURL+'/user/signup';
		var FR= new FileReader();
		$ionicLoading.hide();

		var imgData = $scope.newimage;
		
		if(imgData != ""){
			var postdata = {first_name : fname, last_name : lname, email : email, password : password, user_job_role : user_job_role,img:imgData,phone:phone,price:price};
		} else {
			var postdata =  {first_name : fname, last_name : lname, email : email, password : password, user_job_role : user_job_role,phone:phone,price:price};
		}

		$http.post(link, postdata).then(function successCallback(response) {
			$ionicLoading.hide();
			if(response.data.ResponseCode == true){
				var successsignup = $ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Thankyou for registering with us. You can now login."
				});
				successsignup.then(function(res) {
					$state.go("app.login", {reload: true});
				});
			} else{
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: response.data.Message
				});
			}
		}, function errorCallback(response) {
			$ionicLoading.hide();
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "Due to some technical issues we are unable to process your request. Please try again later."
			});
		});

		
	}

	$scope.removeImage = function(){
		$scope.showimage = false;
		$scope.deleteimage = false;
		$scope.newimage = "";
		$scope.image = "images/add-image.png";
	}

	$scope.openPopover = function() {
		// Show the action sheet
		var showActionSheet = $ionicActionSheet.show({
			buttons: [
			{ text: '<i class="ion-android-image"></i> Gallery',  },
			{ text: '<i class="ion-android-camera"></i> Camera' }
			],

			titleText: 'Choose Image',
			cancelText: 'Cancel',
			cssClass: 'uploadimage',
			cancel: function() {
			// add cancel code...
			},

			buttonClicked: function(index) {
				if(index === 0) { // Gallery
					var type = "gallery";
				} else if(index === 1){ // Camera
					var type = "camera";
				}

				camera.getPicture(type).then(function (imageData) {
					
					if(imageData != ""){
						//alert(imageData);
						$scope.image = imageData;
						$scope.deleteimage = true;
						$scope.showimage = true;
						$scope.newimage = imageData;
					}					
				});

				return true;
			},

			destructiveButtonClicked: function() {
			// add delete code..
			}
		});
	}

	$scope.login=function(){
		$state.go("app.login", null, {reload: true});
	}

	
})