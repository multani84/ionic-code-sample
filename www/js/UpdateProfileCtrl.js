angular.module('starter.controllers').controller('UpdateProfileCtrl', function($scope, $ionicLoading, $stateParams, $state, myURL, $http, localStorageService, $ionicPopup, $ionicActionSheet, camera){

	var userdata = localStorageService.get("userdetails");
	
	console.log(userdata);

	$scope.user = [];

	$scope.user.address				= userdata.address;
	$scope.user.age					= userdata.age;
	$scope.user.created				= userdata.created;
	$scope.user.email				= userdata.email;
	$scope.user.facebook_id			= userdata.facebook_id;
	$scope.user.first_name			= userdata.first_name;
	$scope.user.last_name			= userdata.last_name;
	$scope.user.gender				= userdata.gender;
	$scope.user.id					= userdata.id;
	$scope.user.modified			= userdata.modified;
	$scope.user.password			= userdata.password;
	$scope.user.phone				= userdata.phone;
	$scope.user.price				= Number(userdata.price);
	$scope.user.profile_picture		= userdata.profile_picture;
	$scope.user.status				= userdata.status;
	$scope.user.token_id			= userdata.token_id;
	$scope.user.user_job_role		= userdata.user_job_role;
	$scope.user.user_type			= userdata.user_type;
	$scope.user.reset_password_code	= userdata.reset_password_code;
	$scope.user.age					= userdata.age;
	$scope.user.about				= userdata.about;
	$scope.user.gender_txt = "Select Gender";

	$scope.user.profile_status		= userdata.profile_status;
	
	if(userdata.gender != "" ){
		$scope.user.gender_txt = userdata.gender;
	}

	var user_id = userdata.id;
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}

	$scope.user.newimage = "";

	$scope.collections = [{ name: 'Male', value: 'Male' },
	{ name: 'Female', value: 'Female' }, 
	{ name: 'Other', value: 'Other'}];

	
	if(userdata.gender=="Male"){
		//$scope.user.gender = $scope.collections[0];
		$scope.selectedindex = 0;
		$scope.user.gender_txt = userdata.gender;
	}else if(userdata.gender=="Female"){
		//$scope.user.gender = $scope.collections[1];
		$scope.selectedindex = 1;
		$scope.user.gender_txt = userdata.gender;
	}else{
		//$scope.user.gender = $scope.collections[2];
		$scope.selectedindex = 2;
	}

	if(userdata.user_job_role == 2){
		$scope.user.isChecked = true;
		$scope.jobseekerdiv = true;
	} else {
		$scope.user.isChecked = false;
		$scope.jobseekerdiv = false;
	}


	$scope.updateJobSeeker = function(user){
		if(user.isChecked == true){
			$scope.jobseekerdiv = true;
		} else {
			//$scope.user.isChecked = false;
			$scope.jobseekerdiv = false;
		}
	}
  
	$scope.UpdateProfile=function(user){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});  
			
		if(user.isChecked == true){
			var user_job_role = 2;
		} else {
			var user_job_role = 1;
		}
		
		var api_token = userdata.api_token;

		var link=myURL.sayURL+'/user/updateprofile?api_token='+api_token;
		
		var imgData = $scope.user.newimage;

		var genderuser = $scope.user.gender;
		
		if(imgData != ""){
			var postdata = {first_name : user.first_name, last_name :user.last_name, user_id : user_id, phone : user.phone, user_job_role : user_job_role, address : user.address, img:imgData, age: user.age, gender: user.gender, price: user.price, about: user.about, profile_status: user.profile_status};
		} else {
			var postdata = {first_name : user.first_name, last_name :user.last_name, user_id : user_id, phone : user.phone, user_job_role : user_job_role, address : user.address, age: user.age, gender: user.gender, price: user.price, about: user.about, profile_status: user.profile_status};
		}

		$http.post(link, postdata).then(function successCallback(response) {
			$ionicLoading.hide();

			if(response.data.ResponseCode == true){

				var udetails = response.data.data;

				udetails.profile_picture = response.data.base_url + "" + udetails.profile_picture;

				localStorageService.set("userdetails", udetails);

				if(udetails.user_job_role == 2){
					$scope.$parent.isjobseeker = true;
					$scope.$parent.isjobseekerhistory = true;
				} else {
					$scope.$parent.isjobseeker = false;
					$scope.$parent.isjobseekerhistory = false;
				}

				$scope.$emit('userseeker');

			}

			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: response.data.Message
			});
		}, function errorCallback(response) {
			$ionicLoading.hide();
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "Due to some technical issues we are unable to process your request. Please try again later."
			});
		});
	}

	$scope.removeImage = function(){
		$scope.user.profile_picture = userdata.profile_picture;
		$scope.deleteimage = false;
		$scope.user.newimage = "";
	}

	$scope.openPopover = function() {
		// Show the action sheet
		var showActionSheet = $ionicActionSheet.show({
			buttons: [
			{ text: '<i class="ion-android-image"></i> Gallery',  },
			{ text: '<i class="ion-android-camera"></i> Camera' }
			],

			titleText: 'Choose Image',
			cancelText: 'Cancel',
			cssClass: 'uploadimage',
			cancel: function() {
			// add cancel code...
			},

			buttonClicked: function(index) {
				if(index === 0) { // Gallery
					var type = "gallery";
				} else if(index === 1){ // Camera
					var type = "camera";
				}

				camera.getPicture(type).then(function (imageData) {
					if(imageData != ""){
						$scope.user.profile_picture = imageData;
						$scope.user.newimage = imageData;
						$scope.deleteimage = true;
					}					
				});

				return true;
			},

			destructiveButtonClicked: function() {
			// add delete code..
			}
		});
	};

	$scope.setgender = function(){
		var showGenderType = $ionicPopup.show({
			templateUrl: 'templates/job_type.html',
			title: 'Choose Gender',
			subTitle: '',
			scope: $scope,
			buttons: []
		});

		$scope.showGenderType = showGenderType;
	}

	$scope.jobtypeselected = function(gender, index){
		$scope.user.gender = gender.value;
		$scope.user.gender_txt = gender.name;
		$scope.selectedindex = index;
		$scope.showGenderType.close();
	}
});