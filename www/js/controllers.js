angular.module('starter.controllers', ['ngOpenFB', 'ionic-ratings', 'ngCordova'])


.directive('onFinishRender', function ($timeout) {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			if (scope.$last === true) {
				$timeout(function () {
					scope.$emit('ngRepeatFinished');
				});
			}
		}
	}
})

.filter('ucfirst', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
})

.factory('myURL', function() {
	return {
		sayURL : 'http://externaldeveloper.com/hundelufteren/api'
		//sayURL : 'http://test.vegalms.in/hundelufteren/api'
		
	}
})

.factory('camera', function($cordovaCamera) {
   return {
		getPicture: function (type) {
			if(type == "gallery"){
				var options = {
				  quality: 50,
				  destinationType: Camera.DestinationType.DATA_URL,
				  sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
				  allowEdit: false,
				  encodingType: Camera.EncodingType.JPEG,
				  targetWidth: 800,
				  targetHeight: 800,
				  popoverOptions: CameraPopoverOptions,
				  saveToPhotoAlbum: false,
				  correctOrientation:true
				};
			} else {
				var options = {
					quality: 50,
					destinationType: Camera.DestinationType.DATA_URL,
					sourceType: Camera.PictureSourceType.CAMERA,
					allowEdit: false,
					encodingType: Camera.EncodingType.JPEG,
					targetWidth: 800,
					targetHeight: 800,
					popoverOptions: CameraPopoverOptions,
					saveToPhotoAlbum: false,
					correctOrientation:true
				};
			}

			return $cordovaCamera.getPicture(options).then(function(imageData) {
				return "data:image/jpeg;base64," + imageData;
			}, function(err) {
				return "";
			});
		}
    }
})

.factory('dbservice', function($http) {
    /* job listing */
    
	var domain = 'http://externaldeveloper.com/hundelufteren/';
	
	//var domain = 'https://externaldeveloper.com/hundelufteren/';

	//var domain = 'http://test.vegalms.in/'

	var apiEnd = domain + 'api/';
	
    return {
		apiurl: apiEnd,
		domainurl:domain,
		getSettings: function(){
            return $http.get(apiEnd + 'setting/details').then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getChatMessages: function(myid, profileid, jobid, lastMsgId, api_token){
            return $http.get(apiEnd + 'admin/chat/list/'+myid+"/"+profileid+"/"+jobid+"/"+lastMsgId+"?api_token="+api_token).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getAllMessages: function(profileid, api_token){
            return $http.get(apiEnd + 'admin/chat/alllist/'+profileid+"?api_token="+api_token).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
        getJobDetails: function(jobid, user_id){
            return $http.get(apiEnd + 'jobs/viewjob?job_id='+jobid+'&user_id='+user_id).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getJobSummary: function(jobid, user_id, typeofrequest, api_token){
            return $http.get(apiEnd + 'jobs/jobsummary?job_id='+jobid+'&user_id='+user_id+"&typeofrequest="+typeofrequest+"&api_token="+api_token).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getUserDetails: function(user_id){
            return $http.get(apiEnd + 'user/viewprofile?user_id='+user_id).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getUserReviews: function(user_id){
            return $http.get(apiEnd + 'user/feedbacklist?user_id='+user_id).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		endJob: function(jobid, userid, api_token){
            return $http.post(apiEnd + 'jobs/endjob?api_token='+api_token, {user_id : userid, job_id : jobid}).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		applyUserForJob: function(jobid, userid, api_token){
            return $http.post(apiEnd + 'jobs/applyforjob?api_token='+api_token, {user_id : userid, job_id : jobid}).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		
		directHireUserForJob: function(jobid, userid, api_token){
            return $http.post(apiEnd + 'jobs/hireforjob?api_token='+api_token, {user_id : userid, job_id : jobid}).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },

		hireUserForJob: function(jobid, userid, api_token){
            return $http.post(apiEnd + 'user/hireuser?api_token='+api_token, {user_id : userid, job_id : jobid}).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getSeekers: function(userid, keyword){
			if(keyword != ""){
				var apilink = apiEnd + 'jobs/jobseekers?user_id='+userid+"&keyword="+keyword;
			} else {
				var apilink = apiEnd + 'jobs/jobseekers?user_id='+userid;
			}

            return $http.get(apilink).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getJobHistory: function(user_id, api_token){
            return $http.get(apiEnd + 'jobs/myjobhistory?&user_id='+user_id+"&api_token="+api_token).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getTransactionHistory: function(user_id, api_token){
            return $http.get(apiEnd + 'user/transactionhistory?&user_id='+user_id+"&api_token="+api_token).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getDueAmount: function(jobid, api_token){
            return $http.get(apiEnd + 'job/ownerdueamount?&job_id='+jobid+"&api_token="+api_token).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getJobStatusAndDueAmount: function(jobid, profileid, api_token){
			return $http.get(apiEnd + 'job/jobdetailsanddueamount?&job_id='+jobid+"&user_id="+profileid+"&api_token="+api_token).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
		},
		getPaypalReturnURL: function(jobid, userid, profileid, amount){
			return $http.get('https://externaldeveloper.com/hundelufteren/paypal-adaptive/chained-payment/proccess.php?&job_id='+jobid+"&to_id="+profileid+"&from_id="+userid+"&amount="+amount).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
		},
		updatePaypalPayment: function(pay_key, txn_id){

			return $http.get('https://externaldeveloper.com/hundelufteren/paypal-adaptive/chained-payment/success.php?&pay_key='+pay_key+"&txn_id="+txn_id).then(function(response){
				return response;
            }, function errorCallback(response) {
				return -1;
			});
		},
		getMyJobs: function(user_id, keyword, status, api_token){
			return $http.get(apiEnd + 'jobs/mycreatedjobs?&user_id='+user_id+"&keyword="+keyword + "&status=" + status + "&api_token="+api_token).then(function(response){
				return response;
			}, function errorCallback(response) {
				return -1;
			});
        },
		removeJob: function(user_id, job_id, api_token){
			return $http.post(apiEnd + 'jobs/removejob?api_token='+api_token, {user_id : user_id, job_id : job_id}).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getAppliedJobs: function(user_id, keyword, api_token){
			return $http.get(apiEnd + 'jobs/myappliedjobs?&user_id='+user_id+"&keyword="+keyword+"&api_token="+api_token).then(function(response){
				return response;
			}, function errorCallback(response) {
				return -1;
			});
        },
		changePassword: function(user_id, oldpwd, new_password, api_token){
			return $http.post(apiEnd + 'user/changepassword?api_token='+api_token, {user_id : user_id, oldpassword : oldpwd, newpassword : new_password}).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		resetPassword: function(emailaddress, new_password){
			return $http.post(apiEnd + 'user/newpassword', {email : emailaddress, newpassword : new_password}).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		login: function(emailaddress, password, device_id, devicetype){
			return $http.post(apiEnd + 'user/login', {email: emailaddress, password: password, device_id: device_id, devicetype: devicetype}).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		forgotpassword: function(emailaddress){
			return $http.post(apiEnd + 'user/forgotpassword', {email: emailaddress}).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		fblogin: function(emailaddress, facebook_id, device_id, name, first_name, last_name, picture, gender, devicetype){
			return $http.post(apiEnd + 'user/facebooklogin', {email : emailaddress, facebook_id : facebook_id, device_id: device_id, name: name, first_name: first_name, last_name: last_name, picture: picture, gender: gender, devicetype: devicetype}).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		fbloginCheck: function(facebook_id){
			return $http.post(apiEnd + 'user/checkfbid', {facebook_id: facebook_id}).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getJobCategories: function(){
			return $http.get(apiEnd + 'category/all').then(function(response){
				return response;
			}, function errorCallback(response) {
				return -1;
			});
        },
		addJob: function(postdata, api_token){
			return $http.post(apiEnd + 'jobs/addjob?api_token='+api_token, postdata).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getAllJobs: function(lat, lng, user_id, limit, keyword, category){

		
			return $http.get(apiEnd + 'jobs/joblisting?lat='+lat+'&lng='+lng+'&user_id='+user_id+'&limit='+limit+'&keyword='+keyword+"&category="+category).then(function(response){
				return response;
			}, function errorCallback(response) {
				return -1;
			});

			
        },
		giveFeedback: function(from_user_id, to_user_id, job_id, rating, feedback, api_token){
			return $http.post(apiEnd + 'jobs/addrating?api_token='+api_token, {from_user_id : from_user_id, to_user_id :to_user_id, job_id: job_id, rating: rating, feedback: feedback}).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		addPaymentDetails: function(postdata, api_token){
			return $http.post(apiEnd + 'user/addpaymentdetails?api_token='+api_token, postdata).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		removeAppliedJob: function(jobid, userid, api_token){
            return $http.post(apiEnd + 'jobs/removeapplied?api_token='+api_token, {user_id : userid, job_id : jobid}).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getMessagesCountUsers: function(sender_id, receiver_id, job_id, indexid, api_token){
            return $http.post(apiEnd + 'chat/messages?api_token='+api_token, {sender_id : sender_id, receiver_id : receiver_id, job_id: job_id}).then(function(response){
				response.data.indexid = indexid;
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getJobMessagesCount: function(jobid, owner_id, indexid, api_token){
            return $http.post(apiEnd + 'jobs/jobmessages?api_token='+api_token, {owner_id : owner_id, job_id : jobid}).then(function(response){
				response.data.indexid = indexid;
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getJobApplicantMessagesCount: function(jobid, applicant_id, indexid, api_token){
            return $http.post(apiEnd + 'jobs/jobapplicantmessages?api_token='+api_token, {applicant_id : applicant_id, job_id : jobid}).then(function(response){
				response.data.indexid = indexid;
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        },
		archiveJob: function(user_id, job_id, archive_type, api_token){
			return $http.post(apiEnd + 'archivejobs/add?api_token='+api_token, {user_id : user_id, job_id : job_id, archive_type: archive_type}).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
		},
		unarchiveJob: function(user_id, job_id, archive_type, api_token){
			return $http.post(apiEnd + 'archivejobs/remove?api_token='+api_token, {user_id : user_id, job_id : job_id, archive_type: archive_type}).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
		},
		logout: function(device_id, api_token){
			return $http.post(apiEnd + 'user/logout?api_token='+api_token, {device_id : device_id}).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
		},
		messagenotification: function(messageid, api_token){
			return $http.post(apiEnd + 'user/msgnotification?api_token='+api_token, {messageid : messageid}).then(function(response){
                return response;
            }, function errorCallback(response) {
				return -1;
			});
        },
		getJobApplicants: function(jobid, api_token){
			return $http.get(apiEnd + 'jobs/applicantlist?job_id='+jobid+"&api_token="+api_token).then(function(response){
                return response.data;
            }, function errorCallback(response) {
				return -1;
			});
        }
    }
})

.service('userdetails', function(localStorageService) {
	this.getUserId = function() {
		var userdetails = localStorageService.get("userdetails");
	
		var user_id = 0;

		if(userdetails != null){
			var user_id = userdetails.id;
		}

		return user_id;
	};

	this.getTokenId = function() {
		var userdetails = localStorageService.get("userdetails");
	
		var user_id = 0;

		if(userdetails != null){
			var user_id = userdetails.api_token;
		}

		return user_id;
	};

	this.getLoggedUserDetails = function() {
		var userdetails = localStorageService.get("userdetails");
		
		if(userdetails != null){
			return userdetails;
		}

		return 0;
	};

})

.service('jobdetails', function(){
    var jobdetails;
})

 .factory('Reciever', function () {
	var dataup = {};
	var recieverid={};
	return {
		getDataUp: function () {
			return dataup;
		},
		 getRecieverId: function () {
			return recieverid;
		},
		setDataUp: function (userparameter) {
			dataup = userparameter;
		},
		setRecieverId: function (userparameter) {
			recieverid = userparameter;
		}
	};
})

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $stateParams, $state, $rootScope, localStorageService, $ionicLoading, $ionicHistory, $timeout, dbservice, $cordovaNetwork, userdetails) {

	$scope.bottomtabs = false;
	$scope.showmenubutton = false;

	var userdetails = userdetails.getLoggedUserDetails();

	if(userdetails != 0){
		if(userdetails.user_job_role == 1){
			$scope.isjobseeker = false;
		} else {
			$scope.isjobseeker = true;
		}
		$scope.showloginicon = false;
	} else {
		$scope.showloginicon = true;
	}

	$scope.termlink = function(){
		window.open($rootScope.term_link, '_system');
	}

	$scope.privacylink = function(){
		window.open($rootScope.privacy_link, '_system');
	}

	$rootScope.$on('settingsintialized', function (event, args) {
		$scope.appname = $rootScope.appname;
	});

	$scope.logout = function(){
		
		var api_token = userdetails.api_token;

		var userdetailsdata = localStorageService.get("userdetails");

		var device_id = localStorageService.get("deviceToken");

		localStorageService.clearAll();

		localStorageService.set("deviceToken",device_id);
		
		$scope.showmenubutton = false;

		$scope.showloginicon = true;
		
		var token_id = userdetails.token_id;

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		if(device_id === null){
			device_id = "0";
		} 

		dbservice.logout(device_id, api_token).then(function(response){
			
			$scope.$broadcast('userlogout');

			$ionicLoading.hide();

			$ionicHistory.nextViewOptions({
				disableBack: true
			});


			$state.go("app.map", null, {reload: true}); 
		
			//$timeout(function(){
				//window.location.reload();
			//}, 1000);

		});

	};

	$scope.gotoView = function(view){
		$ionicHistory.nextViewOptions({
			disableAnimate: true,
			disableBack: true,
			historyRoot: true 
		});

		$state.go(view, null); 
	};

	$scope.gotoLogin = function(){
		$state.go("app.login", null, {reload: true}); 
	};

	$scope.$on('userlogin', function (event, data) {
		var userdetailsdata = localStorageService.get("userdetails");
	
		if(userdetailsdata.user_job_role == 2){
			$scope.isjobseeker = true;
		} else {
			$scope.isjobseeker = false;
		}

		$scope.showloginicon = false;
		
		console.log("User Logged In App Controller"); // 'Data to send'
	});

	$scope.$on('userseeker', function (event, data) {
		var userdetailsdata = localStorageService.get("userdetails");
	
		if(userdetailsdata.user_job_role == 2){
			$scope.isjobseeker = true;
		} else {
			$scope.isjobseeker = false;
		}
		
		console.log("User Update Seeker In App Controller"); // 'Data to send'
	});

	$scope.navactive = "map";
	
	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
		if(toState.name == "app.map"){
			$scope.navactive = "map";
		} else if(toState.name == "app.createjob") {
			$scope.navactive = "createjob";
		} else if(toState.name == "app.jobs") {
			$scope.navactive = "alljobs";
		} else if(toState.name == "app.appliedjobs") {
			$scope.navactive = "myjobs";
		} else if(toState.name == "app.myjobs") {
			$scope.navactive = "myposts";
		}
	});
	

	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){

		if(fromState.name != "app.map"){

			if(typeof navigator.connection != 'undefined'){

				var isOffline = $cordovaNetwork.isOffline();

				if(isOffline){
					event.preventDefault();
					if($rootScope.nointernet == 0){
						$rootScope.nointernet = 1;
						var nointernet = $ionicPopup.alert({
							title: 'Internet Disconnected',
							content: "Sorry, no Internet connectivity detected. Please reconnect and try again."
						});

						nointernet.then(function(res) {
							$rootScope.nointernet = 0;
						});
					}
				}
			}
		}

		if($rootScope.pidMessages != null){
			window.clearInterval($rootScope.pidMessages);
		}
	});

})

.controller('NointernetCtrl', function($scope, $state) {
	document.addEventListener("online", function(){
		$state.go("app.map", null, {reload: true});
	}, false);
})

.controller('AccountCtrl', function($scope, userdetails) {
	var user_id = userdetails.getUserId();  
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}
})

.controller('PayRateCtrl', function($scope, $cordovaInAppBrowser, $timeout, $stateParams, $cordovaNetwork, $ionicPopup, $state, userdetails, dbservice, localStorageService, $rootScope, $ionicPopup) {
	
	var profileid = $stateParams.profileid;

	var jobid = $stateParams.jobid;

	var amount = $stateParams.dueAmount;

	var user_id = userdetails.getUserId();  

	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}  
	
	$scope.paypalurl = "";

	$scope.txn_id = 0;

	$scope.paypal_txn_id = 0;

	$scope.amount = amount;

	$scope.commissionrate = $rootScope.commission;

	$scope.mincomm		  = $rootScope.mincommission

	var commissionamt = parseFloat(parseFloat($rootScope.commission)/100) * parseFloat(amount);
	
	var totalamt = commissionamt + parseFloat(amount);

	$scope.totalamt = totalamt;

	$scope.commissionamt = (commissionamt).toFixed(2);

	if(commissionamt < parseFloat($rootScope.mincommission)){
		$scope.comissionshow = false;
		$scope.mincomission = true;
		$scope.commissionamt = $rootScope.mincommission;
	} else {
		$scope.mincomission = false;
		$scope.comissionshow = true;
	}

	$scope.calculateCommission = function(amount){
		var commissionamt = parseFloat(parseFloat($rootScope.commission)/100) * parseFloat(amount);
		
		$scope.commissionrate = $rootScope.commission;

		if(commissionamt < parseFloat($rootScope.mincommission)){
			$scope.comissionshow = false;
			$scope.mincomission = true;
			commissionamt = parseFloat($rootScope.mincommission);
		} else {
			$scope.mincomission = false;
			$scope.comissionshow = true;
		}

		var totalamt = commissionamt + parseFloat(amount);
		$scope.totalamt = totalamt;
		$scope.commissionamt = (commissionamt).toFixed(2);
	}

	
	dbservice.getJobDetails(jobid, profileid).then(function(response){
		if(response.ResponseCode == true){
			$scope.job = response.data;
			$scope.job.amount_due = Number(amount);
		}
	});

	dbservice.getUserDetails(profileid).then(function(response){
		if(response.ResponseCode == true){
			$scope.userdetails = response.data.userDetail;
		}
	});


	$scope.paynow = true;

	$scope.openBrowser = function(job){

		$scope.loadingTrackers = true;
		$scope.paynow = false;

		dbservice.getPaypalReturnURL(jobid, user_id, profileid, job.amount_due).then(function(response){

			if(response.ResponseCode == true){

				var paypalurl = response.data.paypal_url;

				$scope.paypal_txn_id = response.data.paypal_txn_id;

				$scope.txn_id = response.data.txn_id;

				$scope.paypalurl = paypalurl;

				$scope.paynow = true;

				var options = {
					location: 'yes',
					clearcache: 'yes',
					toolbar: 'no',
					closebuttoncaption: 'DONE'
				};

				$cordovaInAppBrowser.open(paypalurl, '_blank', options);

			} else {
				$scope.loadingTrackers = false;
				$scope.paynow = true;
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: response.Message
				});
			}
			
		});		 
	}
	
	$rootScope.$on('$cordovaInAppBrowser:loadstart', function(e, event){

	});

	$rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event){
		var path = event.url;
		var page = path.split("/").pop();

		var domain;
		//find & remove protocol (http, ftp, etc.) and get domain
		if (path.indexOf("://") > -1) {
			domain = path.split('/')[2];
		}
		else {
			domain = path.split('/')[0];
		}

		//find & remove port number
		domain = path.split(':')[0];
	
		if(page == "success.php"){

			$scope.loadingTrackers = false;
			
			
			var pay_key = $scope.paypal_txn_id;

			dbservice.updatePaypalPayment(pay_key, $scope.txn_id).then(function(response){
				$cordovaInAppBrowser.close();
				$state.go("app.transactionhistory", null, {reload: true});
			});
		} else if(page=="cancel.php"){
			$scope.loadingTrackers = false;
			$scope.paynow = true;
			$cordovaInAppBrowser.close();
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "You have cancelled your transaction"
			});
		}
	});
	 

});