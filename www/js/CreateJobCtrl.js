angular.module('starter.controllers').controller('CreateJobCtrl', function($scope, $ionicLoading, $stateParams, $state, $rootScope, userdetails, dbservice, $cordovaDatePicker, ionicDatePicker, $ionicActionSheet, camera, $ionicPopup, $ionicModal) {

	var downindarray=[];

	$scope.job = {job_title: "", sdate: "", edate:"", stime: "", etime: "", job_location: "", description: "", user_price: 0.00, image: ""};

	var user_id = userdetails.getUserId();  
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$state.go("app.login");
	}

	var seekerid = $stateParams.seekerid;
	
	$scope.seekerid = seekerid;

	$scope.uploadimagetxt = "Upload Image";

	$scope.job.billing_type_txt = "Hourly";
	$scope.job.job_category_txt = "Select Job Category";

	$scope.job.billing_type = "hourly";

	if($rootScope.create_job_msg != ""){
		$scope.customjobmessage = true;
		$scope.messagejob = $rootScope.create_job_msg;
	} else {
		$scope.customjobmessage = false;
	}


	$scope.showDatePicker = function(esdate){

		var ipObj1 = {
		  titleLabel: 'Select a Date',
		  closeOnSelect: true,
		  from: new Date(),
		  callback: function (val) {  //Mandatory
			
			var newdate = new Date(val);

			var month = (newdate.getMonth() + 1);

			if(month < 10){
				month = "0" + month;
			}

			var dateselected = newdate.getDate();

			if(dateselected < 10){
				dateselected = "0" + dateselected;
			}

			var datestring = dateselected + '/' + month + '/' +  newdate.getFullYear();
			
			var aajdate = new Date();
			var aajmonth = (aajdate.getMonth() + 1);
			if(aajmonth < 10){
				aajmonth = "0" + aajmonth;
			}
			var aajdateselected = aajdate.getDate();

			if(aajdateselected < 10){
				aajdateselected = "0" + aajdateselected;
			}

			var today = new Date(aajmonth+"/"+aajdateselected+"/"+aajdate.getFullYear());
			
			if (newdate.getTime()<today.getTime()) {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Date can not be earlier than today."
				});
				return false;
			}


			if(esdate == 'edate'){
				$scope.job.edate = datestring;
			} else {
				$scope.job.sdate = datestring;
			}
		  }
		};

		ionicDatePicker.openDatePicker(ipObj1);

		/*var options = {
			date: new Date(),
			mode: 'date', // or 'time'
			minDate: new Date() - 10000,
			allowOldDates: true,
			allowFutureDates: false,
			doneButtonLabel: 'DONE',
			doneButtonColor: '#F2F3F4',
			cancelButtonLabel: 'CANCEL',
			cancelButtonColor: '#000000'
		};

		$cordovaDatePicker.show(options).then(function(date){

			var newdate = new Date(date);
			var datestring = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();

			if(esdate == 'edate'){
				$scope.job.edate = datestring;
			} else {
				$scope.job.sdate = datestring;
			}
			
		});*/
	}


	$scope.showTimePicker = function(esdate){
		var options = {
			date: new Date(),
			mode: 'time', // or 'time'
			allowOldDates: true,
			allowFutureDates: true,
			doneButtonLabel: 'DONE',
			doneButtonColor: '#000000',
			cancelButtonLabel: 'CANCEL',
			cancelButtonColor: '#000000'
		};

		$cordovaDatePicker.show(options).then(function(time){

			var datetext = time.toTimeString();
			// datestring is "20:32:01 GMT+0530 (India Standard Time)"
			// Split with ' ' and we get: ["20:32:01", "GMT+0530", "(India", "Standard", "Time)"]
			// Take the first value from array :)
			datetext = datetext.split(' ')[0];

			if(esdate == 'etime'){
				$scope.job.etime = datetext;
			} else {
				$scope.job.stime = datetext;
			}
		});
	}

	dbservice.getJobCategories().then(function(response){
			
		if(response != -1){

			var inddata =(response.data.data);

			for (var j in inddata){
				downindarray.push(inddata[j]);
			} 
		   
			$scope.jobcategories = downindarray;
		 
			$scope.job.job_categories = $scope.jobcategories[0];

			$ionicModal.fromTemplateUrl('templates/jobcategories.html', {
				scope: $scope,
				animation: 'slide-in-up'
			}).then(function(modal) {
				$scope.jobcategoriesmodal = modal;
			});

		}

	}); 
   
	
	function getLatitudeLongitude(callback, address) {
		// If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
		address = address || 'Ferrol, Galicia, Spain';
		// Initialize the Geocoder
        geocoder = new google.maps.Geocoder();
        if (geocoder) {
            geocoder.geocode({
                'address': address
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    callback(results[0]);
                }else{
                  $scope.job_lat = 0;
                  $scope.job_lng = 0;
                }
            });
        }
	}

	$scope.collections = [
	  { name: 'Hourly', value: 'hourly' }, 
	  { name: 'Fixed', value: 'fixed' }, 
	 // { name: 'Transgender', value: 'Transgender'}
	];

	$scope.job.selectedOption = $scope.collections[0];

	$scope.addjob=function(job){

		if(typeof job.category == 'undefined' || (job.category <= 0 || job.category == "")){
			$ionicPopup.alert({
				cssClass: 'popup-no-title',
				template: "Please select job category."
			});
		} else {

			var startdate = job.sdate;

			var sdates = startdate.split("/");

			var startdate = sdates[2] + "-"+ sdates[1] + "-" +sdates[0];

			var enddate = job.edate;

			var edates = enddate.split("/");

			var enddate = edates[2] + "-"+ edates[1] + "-" +edates[0];
	  
			var address = job.job_location;
			
			var sdate = new Date(sdates[2] + "/"+ sdates[1] + "/" +sdates[0]);
			var edate = new Date(edates[2] + "/"+ edates[1] + "/" +edates[0]);

			var aajdate = new Date();
			var aajmonth = (aajdate.getMonth() + 1);
			if(aajmonth < 10){
				aajmonth = "0" + aajmonth;
			}
			var aajdateselected = aajdate.getDate();

			if(aajdateselected < 10){
				aajdateselected = "0" + aajdateselected;
			}

			var today = new Date(aajdate.getFullYear()+"/"+aajmonth+"/"+aajdateselected);

			var hourstime = Math.abs(Date.parse('01/01/2011 '+job.etime+'') - Date.parse('01/01/2011 '+job.stime+'')) / 36e5;

			//36e5 is the scientific notation for 60*60*1000

			var jobtype = $scope.job.billing_type;
			
			if(sdate.getTime() > edate.getTime()){
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Start date cannot be greater than end date."
				});
				return false;
			} else {
				if(jobtype != "fixed" && jobtype != "Fixed"){

					 if((Date.parse('01/01/2011 '+job.stime+'') >= Date.parse('01/01/2011 '+job.etime+'')) || hourstime<1){
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "Start time should be less than end time. There should be difference of one hour atleast."
						});
						return false;
					} else if(Date.parse('01/01/2011 '+job.stime+'') >= Date.parse('01/01/2011 '+job.etime+'')){
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "Start time should be less than end time."
						});
						return false;
					}
				}
			}

			$ionicLoading.show({
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});

				
			 
			getLatitudeLongitude(function(result){
				if(typeof result.geometry != 'undefined'){
					$scope.job_lat = result.geometry.location.lat();
					$scope.job_lng = result.geometry.location.lng();
				} else {
					$scope.job_lat = 0;
					$scope.job_lng = 0;
				}

				user_id = userdetails.getUserId(); 

				var imgData = $scope.job.image;

				var api_token = userdetails.getTokenId();

				var postdata = {user_id : user_id, cat_id : job.category, job_title : job.job_title, description : job.description, lat : $scope.job_lat, lng:$scope.job_lng, address:job.job_location, start_date: startdate, end_date: enddate, start_time: job.stime, end_time: job.etime, img:imgData, job_type: $scope.job.billing_type, cost: job.user_price};

				dbservice.addJob(postdata, api_token).then(function(response){
					$ionicLoading.hide();
					if(response != -1){
						if(response.data.ResponseCode == true){

							if($scope.seekerid >0){
								
								var jobid = response.data.jobid;
								
								var successjob = $ionicPopup.alert({
									cssClass: 'popup-no-title',
									template: "Your job has been created successfully."
								});

								successjob.then(function(res) {
									dbservice.directHireUserForJob(jobid, $scope.seekerid, api_token).then(function(response){
										//$state.go("app.profilejob", {profileid: $scope.seekerid, jobid: jobid}, {reload: true});
										$state.go("app.singlemypost", {jobId: jobid}, {reload: true});
									});
								});

								
									
							} else {

								var successjob = $ionicPopup.alert({
									cssClass: 'popup-no-title',
									template: "Your job has been created successfully."
								});
								successjob.then(function(res) {
									$state.go("app.myjobs", null, {reload: true});
								});

								
							}

							
						} else {
							$ionicPopup.alert({
								cssClass: 'popup-no-title',
								template: response.data.Message
							});
						}
					} else {
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "Due to some technical issues we are unable to process your request. Please try again later."
						});
					}
				});
				

			}, address);
		}
       
	}

	


	$scope.removeImage = function(){
		$scope.uploadimagetxt = "Upload Image";
		$scope.job.image = "";
		$scope.showimage = false;
		$scope.deleteimage = false;
	}


	$scope.openPopover = function() {
		// Show the action sheet
		var showActionSheet = $ionicActionSheet.show({
			buttons: [
			{ text: '<i class="ion-android-image"></i> Gallery',  },
			{ text: '<i class="ion-android-camera"></i> Camera' }
			],

			titleText: 'Choose Image',
			cancelText: 'Cancel',
			cssClass: 'uploadimage',
			cancel: function() {
			// add cancel code...
			},

			buttonClicked: function(index) {
				if(index === 0) { // Gallery
					
					camera.getPicture('gallery').then(function (imageData) {
						
						if(imageData != ""){
							var image = document.getElementById('myImage');
							$scope.job.image = imageData;
							image.src = imageData;
							$scope.showimage = true;
							$scope.uploadimagetxt = "Change Image";
							$scope.deleteimage = true;
						}					
					});

					return true;

				} else if(index === 1){ // Camera
					
					camera.getPicture('camera').then(function(imageData){
						if(imageData != ""){
							var image = document.getElementById('myImage');
							$scope.job.image = imageData;
							image.src = imageData;
							$scope.showimage = true;
							$scope.uploadimagetxt = "Change Image";
							$scope.deleteimage = true;
						}
					});

					return true;
				}
			},

			destructiveButtonClicked: function() {
			// add delete code..
			}
		});
	}

	

	$scope.billingtype=function(){
		var showBillingType = $ionicPopup.show({
			templateUrl: 'templates/job_type.html',
			title: 'Choose Job Type',
			subTitle: '',
			scope: $scope,
			buttons: []
		});

		$scope.showBillingType = showBillingType;
	};

	$scope.jobtypeselected = function(billingtype){
		$scope.job.billing_type = billingtype.value;
		$scope.job.billing_type_txt = billingtype.name;
		$scope.showBillingType.close();
	};

	$scope.showjobcategories = function(){
		$scope.jobcategoriesmodal.show();
	};

	$scope.closeJobsCategoryModal = function(){
		$scope.jobcategoriesmodal.hide();
	};

	$scope.selectjobcategory = function(category, index){
		$scope.job.category = category.id;
		$scope.job.job_category_txt = category.name;
		$scope.jobcategoriesmodal.hide();
	};

})