angular.module('starter.controllers').controller('JobCtrl', function($scope, $ionicLoading, $stateParams, $ionicNavBarDelegate, $state, $ionicHistory, $ionicPopup, userdetails, $rootScope, dbservice, $timeout) {

	var listarray = [];
	
	var user_id = userdetails.getUserId();
	
	$scope.$on('userlogout', function (event, args) {
		$scope.contentclass = "";
		$scope.$parent.bottomtabs = false;
		$scope.$parent.showmenubutton = false;
	});

	if( user_id >0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
		$scope.contentclass = "has-tabs";
		$scope.$parent.showloginicon = false;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
		$scope.contentclass = "";
		$scope.$parent.showloginicon = true;
	}

	$scope.loadingTrackers = true;

	$scope.editSearch = true;

	$scope.limit = 100;
    
	$scope.search_seeker = "";

	var api_token = userdetails.getTokenId();
	
	$scope.jobcategories = [];

	dbservice.getJobCategories().then(function(response){
		var downindarray = [];

		if(response != -1){

			var inddata =(response.data.data);

			for (var j in inddata){
				downindarray.push(inddata[j]);
			} 
		   
			$scope.jobcategories = downindarray;
			
			

			$timeout(function(){
				$scope.category = "0";
			}, 1000);

		}

	}); 

	$scope.getAllJobs = function(keyword, category){

		user_id = userdetails.getUserId();

		dbservice.getAllJobs(0, 0, user_id, $scope.limit, keyword, category).then(function(response){

			$scope.loadingTrackers = false;

			$ionicLoading.hide();

			if(response != -1){
				var data = response.data.data;
				if(typeof data.length == 'undefined'){
					$scope.items = [];
					$scope.noresults = true;
				} else {
					$scope.noresults = false;
					if(user_id == 0){
						$scope.items = data;
					} else {
						var filteritems = [];
						for(i in data){
							var archivejob = data[i].archivejobs;

							if(archivejob.length === 0){
								filteritems.push(data[i]);
							}
						}

						if(filteritems.length <=0){
							$scope.noresults = true;
							$scope.items = [];
						} else {
							$scope.noresults = false;
							$scope.items = filteritems;
						}
					}
				}
			} else {
				$scope.noresults = true;
				$scope.items = [];
			}
					
		}).finally(function() {
			// Stop the ion-refresher from spinning
			$scope.$broadcast('scroll.refreshComplete');
		});
	}
	
	$scope.getAllJobs("", 0);
	

	$scope.goJob=function(item){
		$state.go("app.singlejob", {jobId: item.id}, {reload: true});
    }

	$scope.SearchJobChange=function(keyword, category){
		
		$scope.category = category;
		$scope.search_seeker = keyword;

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
         
		$scope.getAllJobs(keyword, category);
              
	}

	$scope.archiveJob = function(item, index){
		
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		var archive_type = 1; // 1 for all jobs , 2 for Applied Jobs

		dbservice.archiveJob(user_id, item.id, archive_type, api_token).then(function(response){
			$ionicLoading.hide();
			$scope.items.splice(index, 1);
			
		});

		return true;
	}

	$scope.doRefresh = function() {
		$scope.getAllJobs($scope.search_seeker, $scope.category);
	};
})