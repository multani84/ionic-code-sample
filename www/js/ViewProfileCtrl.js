angular.module('starter.controllers').controller('ViewProfileCtrl', function($scope, $state, $stateParams, $http, $ionicLoading, $ionicPopup, $ionicModal, $ionicListDelegate, $ionicActionSheet, dbservice, userdetails) {
 
	var listarray=[];

	var user_id = userdetails.getUserId();  

	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}
	
	var profileid = $stateParams.profileid;

	var jobid = $stateParams.jobid;

	$scope.showhire = false;
	$scope.showend = false;
	$scope.showpaynow = false;
	$scope.showfeedback = false;
	$scope.showchat = false;
	$scope.showchatnow = false;
	$scope.showhirenow = false;

	var api_token = userdetails.getTokenId();

	if(typeof jobid != 'undefined'){

		$scope.showchatnow = false;
		$scope.showhirenow = false;
		
		var jobstatus = $stateParams.statuscode;

		$scope.showchat = true;

		dbservice.getJobStatusAndDueAmount(jobid, profileid, api_token).then(function(response){
			if(response.ResponseCode== true){ 
				var data = response.data;
				if(data.appliedstatus == 1 && data.jobdetails.status == 3){ // Hired & Completed 
					$scope.showhire = false;
					$scope.showend = false;
					
					$scope.showfeedback = true;

					if(data.ownerdueamount >0){
						$scope.showpaynow = true;
					} else {
						$scope.showpaynow = false;
					}

				} else if(data.appliedstatus == 1){ // Hired
					$scope.showhire = false;
					$scope.showend = true;
					
					$scope.showfeedback = false;

					if(data.ownerdueamount >0){
						$scope.showpaynow = true;
					} else {
						$scope.showpaynow = false;
					}

				} else if(data.jobdetails.status == 1){
					$scope.showhire = true;
					$scope.showend = false;
					$scope.showpaynow = false;
					$scope.showfeedback = false;
				} else {
					$scope.showhire = false;
					$scope.showend = false;
					$scope.showpaynow = false;
					$scope.showfeedback = false;
				}

			} else {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				}); 
			}
		});
		
	} else {
		$scope.showchat = false;
		$scope.showchatnow = true;
		$scope.showhirenow = true;
	}

	$scope.chatnow = function(){
		$state.go("app.chat", {id: profileid, jobid: 0}, {reload: true});
	}



	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	});

	$scope.aboutnav = "active";
	$scope.reviewsnav = "";
	$scope.aboutarea = true;
	$scope.reviewsarea = false;

	$scope.showowneraddressicon = false;
	
	dbservice.getUserDetails(profileid).then(function(response){
		console.log(response);
		$ionicLoading.hide();
		if(response.ResponseCode== true){ 

			var data = response.data.userDetail;

			$scope.loadingTrackers = false;
			$scope.owner = data;

			if(data.address != ""){
				$scope.showowneraddressicon = true;
			}

			var totalratings = 0;
			
			dbservice.getUserReviews(profileid).then(function(response){
				if(response.ResponseCode== true){ 
					$scope.loadingTrackers = false;
					if(response.data.length >0){
						var listreviews = [];
						for (var i in response.data){
							var ratingstars = []; 
							var noratingstars = [];
							response.data[i].stars = [];
							response.data[i].nostars = [];
							for(var j=0;j<response.data[i].rating;j++){
								ratingstars.push(j);
							}
							
							if(response.data[i].rating >0){
								totalratings = totalratings + parseInt(response.data[i].rating);
							}

							var nostar = 5 - parseInt(response.data[i].rating);

							for(var j=0;j<nostar;j++){
								noratingstars.push(j);
							}

							response.data[i].stars = ratingstars; 
							response.data[i].nostars = noratingstars; 
							
							listreviews.push(response.data[i]);
						}

						var userrating = parseInt(totalratings/listreviews.length);

						var userratingstars = [];
						var nouserratingstars = [];

						for(var j=0;j<userrating;j++){
							userratingstars.push(j);
						}
						for(var j=0;j<5-userrating;j++){
							nouserratingstars.push(j);
						}

						$scope.userstars = userratingstars;
						$scope.usernostars = nouserratingstars;

						$scope.items = response.data;
					} else {
						$scope.noreviewsyet = true;
						$scope.items = [];
					}
				} else if(response.ResponseCode== false){
					$scope.noreviewsyet = true;   
				}
			});

			
		} else if(response.ResponseCode== false){
           $state.go("app.map", null, {reload: true});     
        }
	});

	$scope.showtabs = function(tabitem){
		if(tabitem == 1){
			$scope.aboutnav = "";
			$scope.reviewsnav = "active";
			$scope.aboutarea = false;
			$scope.reviewsarea = true;
		} else {
			$scope.aboutnav = "active";
			$scope.reviewsnav = "";
			$scope.aboutarea = true;
			$scope.reviewsarea = false;
		}
	}

	$scope.gotojob = function(jobid){
		$state.go("app.singlejob", {jobId: jobid}, {reload: true}); 
	}

	$scope.EndTask = function(){

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		dbservice.endJob(jobid, user_id, api_token).then(function(response){
			$ionicLoading.hide();
			if(response.ResponseCode==true){
				$scope.showhire = false;
				$scope.showend = false;
				$scope.showpaynow = true;
				$scope.showfeedback = true;
			} else {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: response.Message
				}); 
			}
		});
	}

	$scope.goHire=function(item){
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});
		
		dbservice.hireUserForJob(jobid, profileid, api_token).then(function(response){

			console.log(response);

			$ionicLoading.hide();

			if(response.ResponseCode==true){
				$scope.showhire = false;
				$scope.showend = true;
				$scope.showpaynow = false;
				$scope.showfeedback = false;
			} else {
				$ionicPopup.alert({
					template: response.data.Message
				}); 
			}
		});
    }  

	$scope.SendFeed=function(){
		$state.go("app.feedback",{uid: profileid, jobid: jobid }, {reload: true});
    }

	$scope.payNow=function(){
		var api_token = userdetails.getTokenId();
		dbservice.getDueAmount(jobid, api_token).then(function(response){
			if(response.data.ResponseCode==true){
				$state.go("app.pay_rate",{dueAmount: response.data.data, jobid: jobid, profileid: profileid});
			} else {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: response.data.Message
				});
			}
		});
	}

	$scope.goChat=function(){
		$state.go("app.chat",{ id: profileid, jobid: jobid});
    }
	
	$scope.seekerselectedid = profileid;

	$scope.hireNowPopup = function(){

		$ionicListDelegate.closeOptionButtons();

		var showActionSheet = $ionicActionSheet.show({
			buttons: [
			{ text: 'My Posts',  },
			{ text: 'Create New Job' }
			],

			titleText: 'Select Job Or Create New',
			cancelText: 'Cancel',
			cssClass: 'uploadimage',
			cancel: function() {
			// add cancel code...
			},

			buttonClicked: function(index) {
				if(index === 0) { // My Jobs
					if($scope.jobs.length >0){
						$scope.myjobs.show();
					} else {
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "There are no jobs added by you"
						});
						return true;
					}
				} else if(index === 1){ // Create Job
					$state.go("app.createjobseeker", {seekerid: profileid}, {reload: true});
				}
				

				return true;
			},

			destructiveButtonClicked: function() {
			// add delete code..
			}
		});
	}

	$scope.jobs = [];

	dbservice.getMyJobs(user_id, "", 1, api_token).then(function(response){
		if(response.data.ResponseCode== true){ 
			$scope.jobs = response.data.data;
		} else {
			$scope.jobs = [];
		}
	});

	$ionicModal.fromTemplateUrl('templates/my-posted-jobs-modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.myjobs = modal;
	});

	$scope.closeJobsModal = function() {
		$scope.myjobs.hide();
	};

	$scope.hireJobNow = function(item, index){
		
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		dbservice.applyUserForJob(item.id, profileid, api_token).then(function(response){
			if(response != -1){
					dbservice.hireUserForJob(item.id, $scope.seekerselectedid, api_token).then(function(responsehire){
						if(responsehire != -1){

							if(responsehire.ResponseCode == true){
								$ionicLoading.hide();
								var successhire = $ionicPopup.alert({
									cssClass: 'popup-no-title',
									template: "This user has been hired for this job."
								});

								successhire.then(function(res) {
									$scope.myjobs.hide();
									//$state.go("app.profilejob", {profileid: $scope.seekerselectedid, jobid: item.id}, {reload: true});
									$state.go("app.singlemypost", {jobId: item.id}, {reload: true});
								});


							} else {
								$ionicLoading.hide();
								$ionicPopup.alert({
									cssClass: 'popup-no-title',
									template: "This user is already hired for this job."
								});
							}

						} else {
							$ionicLoading.hide();
							$ionicPopup.alert({
								cssClass: 'popup-no-title',
								template: "Due to some technical issues we are unable to process your request. Please try again later."
							});
						}
					});
			} else {
				$ionicLoading.hide();
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				});
			}
		});

		return true;
	}

})