angular.module('starter.controllers').controller('ChatCtrl', function($scope, $http, $stateParams, Reciever, $ionicScrollDelegate, $ionicHistory, localStorageService, dbservice, $rootScope, userdetails) {
	
	var userdata = localStorageService.get("userdetails");

	var api_token = userdetails.getTokenId();

	var user_id = userdata.id; 

	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$state.go("app.login");
	}

    $scope.sender_id = user_id;

    var uid = 1;

	var history = $ionicHistory.currentView();

	var newURL = history.url;

	var jobid = $stateParams.jobid;

	$scope.jobid = jobid;

	if(jobid >0){
		$scope.headerclass = "chatuser";
	} else {
		$scope.headerclass = "chatuser-nojob";
	}

	$scope.reciver_id = $stateParams.id;

	$scope.job_id = jobid;

	$scope.receiverdetails = [];

	dbservice.getJobDetails(jobid, $scope.reciver_id).then(function(response){		
		if(response != "-1"){	
			if(response.ResponseCode== true){
				$scope.job_title = response.data.job_title;
			}
		}
	});	

  
	$scope.lastMsgId = 0;
	$scope.messages = [];
	$scope.oldmessages = [];

	var uname = 'Amandeep Singh';

	$scope.getLastMessage = function() {
		return $scope.messages[$scope.messages.length - 1];
	};

	$scope.getLastMessageId = function() {
		console.info('------',$scope.getLastMessage());
		if(typeof $scope.getLastMessage() !=='undefined'){
			return $scope.getLastMessage().id;
		}
		else{
			return 0;
		}
	};

	$scope.urlSaveMessage = dbservice.domainurl + 'admin/websavemsg';
    
	$scope.loadingchat = true;

	//$scope.pidMessages = null;
	$scope.pidPingServer = null;

	$scope.beep = new Audio('css/beep.ogg');

	$scope.online = null;
	$scope.lastMessageId = null;
	$scope.historyFromId = null;

	$scope.me = {
		username: uname,
		//username: "<?php echo $chatApp->sanitize($chatApp->getCookie('username')); ?>",
		message: null
	};

	$scope.me.sender_id = $scope.sender_id;
	$scope.me.reciver_id = $scope.reciver_id;
	$scope.me.job_id = $scope.job_id;

	$scope.pageTitleNotificator = {
		vars: {
			originalTitle: window.document.title,
			interval: null,
			status: 0
		},    
		on: function(title, intervalSpeed) {
			var self = this;
			if (! self.vars.status) {
				self.vars.interval = window.setInterval(function() {
					window.document.title = (self.vars.originalTitle == window.document.title) ? 
					title : self.vars.originalTitle;
				},  intervalSpeed || 1000);
				self.vars.status = 1;
			}
		},
		off: function() {
			window.clearInterval(this.vars.interval);
			window.document.title = this.vars.originalTitle;   
			this.vars.status = 0;
		}
	};

	$scope.saveMessage = function(form, callback) {
		//alert('called');
		console.warn('-[-[-',$scope.lastMsgId);
		var data = $.param($scope.me);
		console.info(data);

		//console.warn('-[-[-',data);
		if (! ($scope.me.username && $scope.me.username.trim())) {
		return $scope.openModal();
		}

		if (! ($scope.me.message && $scope.me.message.trim() &&
			$scope.me.username && $scope.me.username.trim())) {
			return;
		}
		$scope.me.message = '';
		
		
		$http({
			method: 'POST',
			url: $scope.urlSaveMessage+"?api_token="+api_token,
			data: data,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(data) {

			$scope.listMessages(true);

			if(data.messageid >0){
				dbservice.messagenotification(data.messageid, api_token);
			}
		
		});

	};

	$scope.replaceShortcodes = function(message) {
		var msg = '';
		msg = message.toString().replace(/(\[img])(.*)(\[\/img])/, "<img src='$2' />");
		msg = msg.toString().replace(/(\[url])(.*)(\[\/url])/, "<a href='$2'>$2</a>");
		return msg;
	};

	$scope.notifyLastMessage = function() {
		if (typeof window.Notification === 'undefined') {
			return;
		}
		window.Notification.requestPermission(function (permission) {
			var lastMessage = $scope.getLastMessage();
			if (permission == 'granted' && lastMessage && lastMessage.username) {
				var notify = new window.Notification(lastMessage.username + ' says:', {
					body: lastMessage.message
				});
				notify.onclick = function() {
					window.focus();
				};
				notify.onclose = function() {
					$scope.pageTitleNotificator.off();
				};
				var timmer = setInterval(function() {
					notify && notify.close();
					typeof timmer !== 'undefined' && window.clearInterval(timmer);
				}, 10000);
			}
		});
	};
		
	$scope.allowajax = 1;

	
	
	$scope.listMessages = function(wasListingForMySubmission) {
		
		if($scope.allowajax == 1){
			$scope.allowajax = 0;
			
			dbservice.getChatMessages($scope.sender_id, $scope.reciver_id, jobid, $scope.lastMsgId, api_token).then(function(data){
				
				$scope.loadingchat = false;

				$scope.allowajax = 1;

				$scope.messages = [];
			
				if(data.length >0){
					angular.forEach(data, function(message) {
						message.message = $scope.replaceShortcodes(message.message);
						//if($scope.lastMsgId==0){
						
						var newdate = new Date(message.date);

						var datearray = message.date.split(" ");

						var hours = newdate.getHours();
						var minutes = newdate.getMinutes();
						var ampm = hours >= 12 ? 'pm' : 'am';
						hours = hours % 12;
						hours = hours ? hours : 12; // the hour '0' should be '12'
						minutes = minutes < 10 ? '0'+minutes : minutes;
						var strTime = hours + ':' + minutes + ' ' + ampm;

						if(message.sender_id == user_id){ // Logged in User Sent Message
							message.profile_picture = userdata.profile_picture;
							message.timeinfo = userdata.first_name + " " + userdata.last_name + " "+ datearray[0] + " " + strTime;
						} else {
							message.profile_picture = $scope.receiver_image;
							message.timeinfo = $scope.receiverdetails.first_name + " " + $scope.receiverdetails.last_name + " "+ datearray[0] + " " + strTime;
						}

						$scope.oldmessages.push(message);

						$scope.messages.push(message);
					});

					//console.log($scope.oldmessages);

					var lastMessage = $scope.getLastMessage();

					console.log(lastMessage);

					var lastMessageId = lastMessage && lastMessage.id;

				   // if ($scope.lastMessageId !== lastMessageId) {
					if (typeof lastMessage.sender_id != 'undefined' && lastMessage.sender_id != user_id) {
						$scope.onNewMessage(false);
						$scope.scrollDown();
					}

					$scope.lastMessageId = lastMessageId;

					if(typeof $scope.getLastMessage() !=='undefined'){
						$scope.lastMsgId = $scope.getLastMessage().id;
					}
				}
		
			});
		}
	};

	$scope.onNewMessage = function(wasListingForMySubmission) {
		if ($scope.lastMessageId && !wasListingForMySubmission) {
			$scope.playAudio();
			//$scope.pageTitleNotificator.on('New message');
			//$scope.notifyLastMessage();
		}

		
		/*window.addEventListener('focus', function() {
			$scope.pageTitleNotificator.off();
		});*/
	};

 

	$scope.init = function() {
		//alert('wrking');
		$scope.listMessages();
		$rootScope.pidMessages = window.setInterval($scope.listMessages, 10000);
	};

	$scope.scrollDown = function() {
		$ionicScrollDelegate.scrollBottom();
	};

	$scope.clearHistory = function() {
		var lastMessage = $scope.getLastMessage();
		var lastMessageId = lastMessage && lastMessage.id;
		lastMessageId && ($scope.historyFromId = lastMessageId);
	};

	$scope.openModal = function() {
		$('#choose-name').modal('show');
	};

	$scope.playAudio = function() {
		$scope.beep && $scope.beep.play();
	};

	dbservice.getUserDetails($scope.reciver_id).then(function(response){		
		if(response != "-1"){	
			if(response.ResponseCode== true){
				$scope.username = response.data.userDetail.first_name + " " + response.data.userDetail.last_name;
				$scope.receiver_image = response.data.userDetail.profile_picture;
				$scope.receiverdetails = response.data.userDetail;
				$scope.init();

			}
		}
	});

    
})