angular.module('starter.controllers').controller('SeekerCtrl', function($scope, $ionicLoading, $stateParams, $state, dbservice, userdetails, $ionicActionSheet, $ionicListDelegate, $ionicModal, $ionicPopup) {
	
	var user_id = userdetails.getUserId();  
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}


	var keyword = ""; 

	dbservice.getSeekers(user_id, keyword).then(function(response){
		if(response.ResponseCode== true){ 
			var data = response.data;
			$scope.loadingTrackers = false;
			$scope.items = data;
			if(typeof data.length == 'undefined'){
				$scope.noresults = true;
			} else {
				$scope.noresults = false;
			}
		} else if(response.ResponseCode== false){
			$scope.noresults = true;
		}
	});

	$scope.viewProfile = function(seeker){
		$state.go("app.profile", {profileid: seeker}, {reload: true});
	}

	$scope.chatnow = function(item){
		$state.go("app.chat", {id: item.id, jobid: 0}, {reload: true});
	}

	$scope.seekerselectedid = 0;

	$scope.hirenow = function(item){

		$ionicListDelegate.closeOptionButtons();

		var showActionSheet = $ionicActionSheet.show({
			buttons: [
			{ text: 'My Posts',  },
			{ text: 'Create New Job' }
			],

			titleText: 'Select Job Or Create New',
			cancelText: 'Cancel',
			cssClass: 'uploadimage',
			cancel: function() {
			// add cancel code...
			},

			buttonClicked: function(index) {
				if(index === 0) { // My Jobs
					if($scope.jobs.length >0){
						$scope.seekerselectedid = item.id;
						$scope.myjobs.show();
					} else {
						$ionicPopup.alert({
							cssClass: 'popup-no-title',
							template: "There are no jobs added by you"
						});
						return true;
					}
				} else if(index === 1){ // Create Job
					$state.go("app.createjobseeker", {seekerid: item.id}, {reload: true});
				}
				

				return true;
			},

			destructiveButtonClicked: function() {
			// add delete code..
			}
		});
	}

	$scope.jobs = [];
	
	var api_token = userdetails.getTokenId();

	dbservice.getMyJobs(user_id, keyword, 1, api_token).then(function(response){
		if(response.data.ResponseCode== true){ 
			$scope.jobs = response.data.data;
		} else {
			$scope.jobs = [];
		}
	});

	$ionicModal.fromTemplateUrl('templates/my-posted-jobs-modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.myjobs = modal;
	});

	$scope.closeJobsModal = function() {
		$scope.myjobs.hide();
	};

	$scope.hireJobNow = function(item, index){
		
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		dbservice.applyUserForJob(item.id, $scope.seekerselectedid, api_token).then(function(response){
			if(response != -1){
					dbservice.hireUserForJob(item.id, $scope.seekerselectedid, api_token).then(function(responsehire){
						if(responsehire != -1){

							if(responsehire.ResponseCode == true){
								$ionicLoading.hide();
								var successhire = $ionicPopup.alert({
									cssClass: 'popup-no-title',
									template: "This user has been hired for this job."
								});

								successhire.then(function(res) {
									$scope.myjobs.hide();
									//$state.go("app.profilejob", {profileid: $scope.seekerselectedid, jobid: item.id}, {reload: true});
									$state.go("app.singlemypost", {jobId: item.id}, {reload: true});
								});


							} else {
								$ionicLoading.hide();
								$ionicPopup.alert({
									cssClass: 'popup-no-title',
									template: "This user is already hired for this job."
								});
							}

						} else {
							$ionicLoading.hide();
							$ionicPopup.alert({
								cssClass: 'popup-no-title',
								template: "Due to some technical issues we are unable to process your request. Please try again later."
							});
						}
					});
			} else {
				$ionicLoading.hide();
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				});
			}
		});

		return true;
	}

	

	$scope.SearchSeekerChange=function(keyword){

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		$scope.items = [];
		
		dbservice.getSeekers(user_id, keyword).then(function(response){
			$ionicLoading.hide();
			if(response.ResponseCode== true){ 
				var data = response.data;
				$scope.loadingTrackers = false;
				$scope.items = data;
			
				if(typeof data.length == 'undefined'){
					$scope.noresults = true;
				} else {
					$scope.noresults = false;
				}
				
			} else if(response.ResponseCode== false){
				$scope.noresults = true;
			}
		});
	} 
})