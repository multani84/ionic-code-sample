angular.module('starter.controllers').controller('PayDetailsCtrl', function($scope, $state, dbservice, $rootScope, userdetails, $ionicPopup){
	
	var user_id = userdetails.getUserId();  
	
	if( user_id > 0){
		$scope.$parent.showmenubutton = true;
		$scope.$parent.bottomtabs = true;
	} else {
		$scope.$parent.showmenubutton = false;
		$scope.$parent.bottomtabs = false;
	}

	$scope.collections = [
	  { name: 'BankDetails', value: 'BankDetails' }, 
	  { name: 'Paypal', value: 'Paypal' }
	  
	];
	
	$scope.selectedOptionn = $scope.collections[0];
	
	
	$scope.loadingPaymentDetails = true;
	$scope.paymentform = false;

	$scope.pay = [];

	dbservice.getUserDetails(user_id).then(function(response){
		if(response != -1){
			$scope.loadingPaymentDetails = false;
			$scope.paymentform = true;
			if(response.ResponseCode == true){
				var paymentdetails = response.data.userPaymentDetails;

				$scope.bankdata = false;
				$scope.paypaldata = true;

				if( paymentdetails == null || paymentdetails.paypal_emailid != ""){
					/* $scope.bankdata = false;
					$scope.paypaldata = true;*/
					if(paymentdetails != null){
						$scope.pay.paypalemail = paymentdetails.paypal_emailid;
					}
					$scope.selectedindex = "Paypal";
					$scope.pay.type_txt = "Paypal"; 
				} else {
					/*$scope.bankdata = true;
					$scope.paypaldata = false;*/
					

					$scope.selectedindex = "BankDetails";
					$scope.pay.type_txt = "BankDetails";
	
				}

				if( paymentdetails != null){

					$scope.pay.paypalemail = paymentdetails.paypal_emailid;

					$scope.pay.name = paymentdetails.beneficiary_name;
					$scope.pay.benaddress = paymentdetails.beneficiary_address;
					$scope.pay.bankname = paymentdetails.bank_name;
					$scope.pay.banklocation = paymentdetails.bank_location;
					$scope.pay.bankaccount = paymentdetails.account_number;
					
					$scope.pay.ifsccode = paymentdetails.ifsc_code;
					$scope.pay.routingno = paymentdetails.bank_routing_number;
				}

			}

			console.log($scope);
		}
	});

	$scope.addpayment_details=function(pay, type){
	

		//if(type == "Paypal" || type == "paypal"){
			var postdata = {user_id : user_id, beneficiary_name : "", beneficiary_address : "", bank_name : "", bank_location : "", account_number : "", ifsc_code : "", bank_routing_number : "", paypal_emailid:pay.paypalemail, type: "paypal"};
		//} else {
			//var postdata = {user_id : user_id, beneficiary_name : pay.name, beneficiary_address : pay.benaddress, bank_name : pay.bankname, bank_location : pay.banklocation, account_number : pay.bankaccount, ifsc_code : pay.ifsccode, bank_routing_number : pay.routingno, paypal_emailid: "", type: "bank"};
		//}

		var api_token = userdetails.getTokenId();

		dbservice.addPaymentDetails(postdata, api_token).then(function (response){
			if(response != -1){
				var data = response;
				
				if(data.ResponseCode==true){
					var successsave = $ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: data.Message
					});
					successsave.then(function(res) {
						$state.go("app.map");
					});
				} else {
					$ionicPopup.alert({
						cssClass: 'popup-no-title',
						template: "Due to some technical issues we are unable to process your request. Please try again later."
					});
				}
			} else {
				$ionicPopup.alert({
					cssClass: 'popup-no-title',
					template: "Due to some technical issues we are unable to process your request. Please try again later."
				});
			}
			
		}); 
	
	}

	$scope.paytype=function(){
		var showBillingType = $ionicPopup.show({
			templateUrl: 'templates/job_type.html',
			title: 'Choose Pay Type',
			subTitle: '',
			scope: $scope,
			buttons: []
		});

		$scope.showBillingType = showBillingType;
	};

	$scope.jobtypeselected = function(billingtype){
		$scope.selectedindex = billingtype.value;
		$scope.pay.type = billingtype.value;
		$scope.pay.type_txt = billingtype.name;
		$scope.showBillingType.close();

		if(billingtype.value == "Paypal"){
			$scope.bankdata = false;
			$scope.paypaldata = true;
			
		}else{
			$scope.paypaldata = false;
			$scope.bankdata = true;
		}
	}
})